package com.easwareapps.daq.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.easwareapps.daq.MainActivity;
import com.easwareapps.daq.R;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.adapter.InstalledAppsAdapter;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * EA-BulkReinstaller
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class InstalledAppsFragment extends Fragment {

    ArrayList<PInfo> res;
    RecyclerView apps;
    InstalledAppsAdapter allAppsAdapter;
    InstalledAppsAdapter recentAppsAdapter;
    InstalledAppsAdapter disabledAppsAdapter;
    public String searchText = "";
    public boolean isSearchViewExpanded = false;
    static int iconSize = -1;
    MainActivity mainActivity;
    int type = -1;
    DBHelper db;
    AppsLoader appsLoader = null;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_installed_apps, container, false);
        apps = (RecyclerView) rootView.findViewById(R.id.appsList);

//        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
//        llm.setOrientation(LinearLayoutManager.VERTICAL);
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 4);
        apps.setLayoutManager(glm);
        getIconSize();
        new AppsLoader().execute("");

        return rootView;
    }

    public void reloadApps(String search, boolean force) {
        //apps.setAdapter(null);
        if(appsLoader != null) {
            try {
                appsLoader.cancel(true);
            } catch (Exception e) {

            }
        }

        appsLoader = new AppsLoader();
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 4);

        apps.setLayoutManager(glm);
        if (force)
            appsLoader.execute(search, "");
        else
            appsLoader.execute(search);


    }

    public static InstalledAppsFragment getInstance(MainActivity ma, int type) {
        InstalledAppsFragment iaf = new InstalledAppsFragment();
        iaf.mainActivity = ma;
        iaf.type = type;
        return iaf;
    }

    public ArrayList<PInfo> getPackages() {
        if(type == MainActivity.ALL_APPS){
            return  allAppsAdapter.getPackages();
        } else {
            return  recentAppsAdapter.getPackages();
        }

    }




    private class AppsLoader extends AsyncTask<String, Void, Void> {
        boolean forceReload = false;
        @Override
        protected void onPreExecute() {
            Log.d("TIME", System.currentTimeMillis() + " !");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... data) {
            forceReload =  (data.length == 2);
            getApps(data[0]);
            return null;
        }

        void getApps(String search) {
            search = search.trim();
            if(db == null) {
                db = new DBHelper(getActivity());
            }

            if(!forceReload) {
                res = db.getAllApps(search, MainActivity.showSystemApp);
                if(type == MainActivity.RECENT_APPS) {
                    res = db.getRecentApps(search, MainActivity.showSystemApp);
                    return;
                }
                if (res.size() > 0)
                    return;
                else if(!search.equals("")) {
                    return;
                }

            }
            res = new ArrayList<>();
            Log.d("SEARCHING", search + " !!");
            PackageManager pm = getActivity().getPackageManager();
            boolean searchNeeded = !search.equals("");
            int flags = PackageManager.GET_META_DATA |
                    PackageManager.GET_SHARED_LIBRARY_FILES |
                    PackageManager.GET_GIDS ;
            List<PackageInfo> packs;
//            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
//
//            else {
//                flags = flags | PackageManager.;
//                packs = pm.getInstalledPackages(flags);
//            }

            packs = pm.getInstalledPackages(flags);


            for (PackageInfo p : packs) {
                try {
                    PInfo newInfo = new PInfo();
                    newInfo.appName = p.applicationInfo.loadLabel(getActivity()
                            .getPackageManager()).toString();
                    if(searchNeeded && !newInfo.appName.toLowerCase()
                            .contains(search.toLowerCase())) {
                        Log.d("NAME", newInfo.appName.toLowerCase());
                        continue;
                    }
                    newInfo.packageName = p.packageName;
                    ApplicationInfo ai = pm.getApplicationInfo(p.packageName, 0);
                    newInfo.isSystemApp = ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
                    res.add(newInfo);

                } catch (Exception e) {

                    Log.d("Package Exception", e.getLocalizedMessage());
                }
            }
            Collections.sort(res, new Comparator<PInfo>() {

                public int compare(PInfo p1, PInfo p2) {
                    return p1.appName.compareToIgnoreCase(p2.appName);
                }
            });
            db.addAppsToDB(res);
            res = db.getAllApps(search, MainActivity.showSystemApp);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("TIME", System.currentTimeMillis() + " !");
            super.onPostExecute(aVoid);
            switch (type) {
                case MainActivity.ALL_APPS:

                    allAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps, getActivity(),
                            mainActivity, type);
                    apps.setAdapter(allAppsAdapter);
                    allAppsAdapter.requestUpdate();
                    break;
                case MainActivity.RECENT_APPS:

                    recentAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps, getActivity(),
                            mainActivity, type);
                    apps.setAdapter(recentAppsAdapter);
                    recentAppsAdapter.requestUpdate();
                    break;
                case MainActivity.DISABLED_APPS:

                    disabledAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps, getActivity(),
                            mainActivity, type);
                    apps.setAdapter(disabledAppsAdapter);
                    disabledAppsAdapter.requestUpdate();
                    break;
            }


        }
    }


    private int getIconSize() {
        if (iconSize == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            final int width = (dm.widthPixels > dm.heightPixels) ? dm.heightPixels : dm.widthPixels;
            iconSize = width / 5;
        }
        return iconSize;
    }

    public void closeActionMode() {
        if(type == MainActivity.ALL_APPS)
        allAppsAdapter.closeActionMode();
        else if(type == MainActivity.RECENT_APPS)
            recentAppsAdapter.closeActionMode();
    }




    public void removeApp(String packageName) {
        switch (type) {
            case MainActivity.ALL_APPS:
                allAppsAdapter.removeApp(packageName);
                break;
            case MainActivity.RECENT_APPS:
                recentAppsAdapter.removeApp(packageName);
                break;
        }
    }

    public void update(String packageName) {
        try {
            switch (type) {
                case MainActivity.ALL_APPS:
                    allAppsAdapter.updateApp(packageName);
                    break;
                case MainActivity.RECENT_APPS:
                    recentAppsAdapter.updateApp(packageName);
                    break;
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }


}
