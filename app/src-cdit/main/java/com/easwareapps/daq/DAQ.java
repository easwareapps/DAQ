package com.easwareapps.daq;

import android.app.Application;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p>
 * <p>
 * DAQ
 * Created on 26/9/17.
 */

import android.app.Application;


import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
        mode = ReportingInteractionMode.DIALOG,
        resDialogText = R.string.crash_text,
        resDialogIcon = R.mipmap.ic_launcher,
        resDialogTitle = R.string.crash_title,
        resDialogCommentPrompt = R.string.crash_comment_prompt,
        resDialogOkToast = R.string.thanks_for_crash_report,
        mailTo = "info@easwareapps.com",
        customReportContent = { ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
                ReportField.CUSTOM_DATA,
                ReportField.STACK_TRACE, ReportField.LOGCAT }
)

public class DAQ extends Application {
    @Override
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
    }
}
