package com.easwareapps.daq;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.easwareapps.daq.adapter.InstalledAppsAdapter;
import com.easwareapps.daq.fragments.InstalledAppsFragment;
import com.easwareapps.daq.service.EANotificationManager;
import com.easwareapps.daq.utils.Common;
import com.easwareapps.daq.utils.DaqPref;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.EALib;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.utils.PackageStateChanger;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
        implements InstalledAppsAdapter.ActionsTriggeredListener {


    private static final int SETTINGS_REQ = 1000;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static boolean showSystemApp = false;
    public static final int ALL_APPS = 0;
    public static final int RECENT_APPS = 1;
    public static final int DISABLED_APPS = 2;


    InstalledAppsFragment allAppsFragment = null;
    InstalledAppsFragment recentAppsFragment = null;

    private int theme;
    private String oldBackupDir = "";
    private  SearchView searchView;






    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        DaqPref pref = DaqPref.getInstance(getApplicationContext());
        theme = pref.getTheme(false);
        oldBackupDir = pref.getBackupDir();
        showSystemApp = pref.getPref(DaqPref.SHOW_SYSTEM_APP, false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Common.mainActivity = this;

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar != null)
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        allAppsFragment = InstalledAppsFragment.getInstance(MainActivity.this,
                MainActivity.ALL_APPS);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                try {
                    if(position == ALL_APPS) {
                        if(!allAppsFragment.searchText.equals("")) {
                            searchMenuItem.expandActionView();
                        } else {
                            searchMenuItem.collapseActionView();
                        }
                        if(allAppsFragment.isSearchViewExpanded)
                            searchMenuItem.expandActionView();
                        else
                            searchMenuItem.collapseActionView();
                        searchView.setQuery(allAppsFragment.searchText, true);
                    }
                    else if(position == RECENT_APPS) {
                        if(!recentAppsFragment.searchText.equals("")) {
                            searchMenuItem.expandActionView();
                        } else {
                            searchMenuItem.collapseActionView();
                        }
                        searchView.setQuery(recentAppsFragment.searchText, true);
                        if(recentAppsFragment.isSearchViewExpanded)
                            searchMenuItem.expandActionView();
                        else
                            searchMenuItem.collapseActionView();
                    }
                    //searchMenuItem.collapseActionView();
                    //searchView.
                } catch (Exception e) {
                    e.getLocalizedMessage();
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);








    }


    MenuItem searchMenuItem;
    MenuItem toggleSystemApp;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        toggleSystemApp = menu.findItem(R.id.action_toggle_system_app_visibility);
        changeMenuText(toggleSystemApp);
        searchMenuItem = menu.findItem(R.id.search);
        if(searchView == null)
            searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (mViewPager.getCurrentItem() == ALL_APPS) {
                    if(!allAppsFragment.searchText.equals(newText)) {
                        allAppsFragment.searchText = newText;
                        allAppsFragment.reloadApps(newText, false);
                    }
                } else {
                    if(!recentAppsFragment.searchText.equals(newText)) {
                        recentAppsFragment.searchText = newText;
                        recentAppsFragment.reloadApps(newText, false);
                    }
                }


                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if(mViewPager.getCurrentItem() == ALL_APPS) {
                    allAppsFragment.isSearchViewExpanded = false;
                } else if  (mViewPager.getCurrentItem() == RECENT_APPS) {
                    recentAppsFragment.isSearchViewExpanded = false;
                }
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mViewPager.getCurrentItem() == ALL_APPS) {
                    allAppsFragment.isSearchViewExpanded = true;
                } else  if(mViewPager.getCurrentItem() == RECENT_APPS) {
                    recentAppsFragment.isSearchViewExpanded = true;
                }
            }
        });
//        searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem menuItem) {
//
//                return false;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
//
//                return false;
//            }
//        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivityForResult(settings, SETTINGS_REQ);
            return true;
        }

        else if (id == R.id.action_toggle_system_app_visibility) {
            showSystemApp = !showSystemApp;
            if(allAppsFragment != null) {
                allAppsFragment.reloadApps("", false);
            }
            changeMenuText(item);

        }
        else if (id == R.id.action_refresh) {
            if (mViewPager.getCurrentItem() == 0) {
                if(allAppsFragment == null) {
                    allAppsFragment = InstalledAppsFragment
                            .getInstance(MainActivity.this, ALL_APPS);

                }
                allAppsFragment.reloadApps("", true);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_REQ) {

            if(theme != DaqPref.getInstance(getApplicationContext()).getTheme(false)) {
                //MainActivity.this.recreate();
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
                finish();
            }
            DaqPref pref = DaqPref.getInstance(getApplicationContext());
            if (!oldBackupDir.equals(pref.getBackupDir())) {

            }
            if(getSharedPreferences(Common.PACKAGE_NAME, MODE_PRIVATE)
                    .getBoolean(Common.SHOW_SYSTEM_APPS, false) != MainActivity.showSystemApp) {
                showSystemApp = getSharedPreferences(Common.PACKAGE_NAME, MODE_PRIVATE)
                        .getBoolean(Common.SHOW_SYSTEM_APPS, false);
                try {
                    allAppsFragment.reloadApps(searchView.getQuery().toString(), showSystemApp);
                    recentAppsFragment.reloadApps(searchView.getQuery().toString(), showSystemApp);
                    changeMenuText(searchMenuItem);
                } catch (Exception e) {

                }
            }


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void changeMenuText(MenuItem mi) {
        if(showSystemApp) {
            mi.setTitle(R.string.hide_system_apps);
        }else {
            mi.setTitle(R.string.show_system_apps);
        }
    }

    @Override
    public boolean changeAppState(String packageName, String appName) {
        boolean newState = true;
        try {
            PackageManager pm = getPackageManager();
            PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            newState = !p.applicationInfo.enabled;
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        PackageStateChanger psc = new PackageStateChanger();
        boolean result = psc.changeAppState(getApplicationContext(), packageName, newState);
        if(newState) {
            try {
                DBHelper db = new DBHelper(getApplicationContext());
                new EANotificationManager().showNotification(getApplicationContext(),
                        packageName, appName, db.getIdOfApp(packageName));
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {

                PackageManager pm = getPackageManager();
                Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
                launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
                launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(launchIntent);

            } catch (Exception e) {
                e.getLocalizedMessage();
            }
            

        }

        DBHelper db = new DBHelper(getApplicationContext());
        db.updateRecentApp(packageName);
        db.close();
        if(recentAppsFragment != null) {
            recentAppsFragment.reloadApps("", showSystemApp);
        }
        return result;
    }

    AlertDialog ad;
    @Override
    public void confirmAndUninstallApps(final ArrayList<PInfo> packages) {
        int count = 0;
        int noSysApps =0;
        for(PInfo pInfo:packages) {
            if (pInfo.selected) {
               count++;
                if(pInfo.isSystemApp) noSysApps++;
            }
        }
        Log.d("SYSAPPS=", String.valueOf(noSysApps));
        String extraMsg = "";
        if(noSysApps > 0) {
            extraMsg = getString(R.string.contains_system_app, noSysApps);
        }
        if(count == 0 )return;
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
        adb.setTitle(R.string.uninstall_app);
        adb.setMessage(String.format(getString(R.string.desc_uninstall_app), count)
                + extraMsg);
        adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ad.dismiss();
                uninstallPackages(packages);
            }
        });
        adb.setNegativeButton(R.string.cancel, null);
        ad = adb.show();
        closeSelectionMode();
    }

    private void closeSelectionMode() {
        FloatingActionsMenu fab = (FloatingActionsMenu) findViewById(R.id.fab);
        fab.collapse();
        fab.setVisibility(View.GONE);
        if(mViewPager.getCurrentItem() == ALL_APPS) {
            //allAppsFragment.clearSelection();
            allAppsFragment.closeActionMode();
        } else {
            //recentAppsFragment.clearSelection();
            recentAppsFragment.closeActionMode();
        }
    }

    @Override
    public void createShortcuts(ArrayList<PInfo> packages) {

        for(PInfo pInfo:packages) {
            if(!pInfo.selected) continue;
            try {
                String packageName = pInfo.packageName;
                String title = pInfo.appName;
                Intent shortcutIntent = new Intent(getApplicationContext(),
                        QuickChangeActivity.class);
                shortcutIntent.putExtra("packageName", packageName);
                //shortcutIntent.putExtra("newState", newState);
                shortcutIntent.setAction(Intent.ACTION_MAIN);

                Intent addIntent = new Intent();
                addIntent
                        .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
                Bitmap bmp = new EALib().getIcon(getApplicationContext(), packageName);
                Bitmap bmp1 = new EALib().getIcon(getApplicationContext(), "com.easwareapps.daq");

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(
                        new EALib().createCustomIcon(bmp, bmp1), 72, 72, true);
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, scaledBitmap);

                addIntent
                        .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                getApplicationContext().sendBroadcast(addIntent);
            } catch (Exception e) {
                e.getLocalizedMessage();
            }
        }
        closeSelectionMode();

    }



    private void uninstallPackages(ArrayList<PInfo> packages) {
        final ProgressDialog pd = new ProgressDialog(MainActivity.this);
        pd.setTitle("Uninstalling Apps");
        pd.setMessage("Please wait...");
        pd.show();
        try {
            for (PInfo pInfo : packages) {
                if (pInfo.selected) {
                    final String packageName = pInfo.packageName;
                    PackageStateChanger psc = new PackageStateChanger();
                    try {

                        Log.d("Uninstall", packageName);
                        if (psc.executeCommand("pm uninstall " + packageName)) {
                            DBHelper db = new DBHelper(getApplicationContext());
                            db.removeApp(packageName);
                            db.close();
                            allAppsFragment.removeApp(packageName);
                            recentAppsFragment.removeApp(packageName);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        e.getLocalizedMessage();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pd.dismiss();
        closeSelectionMode();
    }






    public void update(String packageName) {
        if(allAppsFragment != null) {
            try {
                allAppsFragment.update(packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } if(recentAppsFragment != null) {
            try {
                recentAppsFragment.update(packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    if (allAppsFragment == null)
                        allAppsFragment = InstalledAppsFragment.getInstance(MainActivity.this,
                                MainActivity.ALL_APPS);

                    return allAppsFragment;
                case 1:
                    if (recentAppsFragment == null)
                        recentAppsFragment = InstalledAppsFragment.getInstance(MainActivity.this,
                                MainActivity.RECENT_APPS);

                    return recentAppsFragment;

            }

            return null;


        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.all);
                case 1:
                    return getResources().getString(R.string.recent);
                case 2:
                    return getResources().getString(R.string.disabled);
            }
            return null;
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        addShortcut("com.easwareapps.baria", true, "ENABLE BARIA");
//        addShortcut("com.easwareapps.baria", false, "DISABLE BARIA");
    }

    @Override
    protected void onResume() {
//        try {
//            if(allAppsFragment != null)
//            allAppsFragment.reloadApps(allAppsFragment.searchText, false);
//        } catch (Exception e) {
//
//        }
//
//        try {
//            if(recentAppsFragment != null)
//                recentAppsFragment.reloadApps(recentAppsFragment.searchText, false);
//        } catch (Exception e) {
//
//        }
        super.onResume();
    }

    private ArrayList<PInfo> getPackages() {
        ArrayList<PInfo> packages = new ArrayList<>();
        if(mViewPager.getCurrentItem() == ALL_APPS) {
            packages = allAppsFragment.getPackages();

        } else if (mViewPager.getCurrentItem() == RECENT_APPS) {
            packages = recentAppsFragment.getPackages();
        }
        return packages;
    }
    public void uninstallSelectedPackages(View view) {
        ArrayList<PInfo> packages = getPackages();
        confirmAndUninstallApps(packages);
    }

    public void createShortcutForSelectedPackages(View view) {
        ArrayList<PInfo> packages = getPackages();
        createShortcuts(packages);

    }

    public void toggleStateOfSelectedPackages(View view) {

        ArrayList<PInfo> packages = getPackages();
        createShortcuts(packages);

    }


}
