package com.easwareapps.daq.adapter;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.ActionMode;
import android.support.v7.widget.RecyclerView;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easwareapps.daq.utils.DaqPref;
import com.easwareapps.daq.utils.BitmapManager;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.R;
import com.easwareapps.daq.utils.PackageStateChanger;

import java.util.ArrayList;


public class InstalledAppsAdapter extends RecyclerView.Adapter<InstalledAppsAdapter.ViewHolder>  {


    private boolean selectionStarted = false;
    private Context context;
    private ArrayList<PInfo> appsDetails;
    public static LruCache<String, Bitmap> cache;
    private int iconSize;
    private static InstalledAppsAdapter instance = null;
    private static RecyclerView rv;
    ActionMode actionMode;
    static FragmentActivity activity;
    DaqPref pref;
    private ActionsTriggeredListener actionsTriggeredListener;
    private int type;
    //private PackageManager pm;
    //Bitmap disableBitmap = null;




    public static InstalledAppsAdapter getInstance(ArrayList<PInfo> apps, Context context,
                                                   int iconSize, RecyclerView rv1, FragmentActivity fa,
                                                   ActionsTriggeredListener actionsTriggeredListener, int type) {
        activity = fa;
        instance = new InstalledAppsAdapter();
        rv = rv1;
        //instance.disableBitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_disabled);

        instance.pref = DaqPref.getInstance(context);
        instance.appsDetails = apps;
        instance.iconSize = iconSize;
        instance.context = context;
        instance.actionsTriggeredListener = actionsTriggeredListener;
        instance.type = type;
        //instance.pm =

        return instance;
    }

    public InstalledAppsAdapter(){

        final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
        int cacheSize = maxMemory/8;
        cache = new LruCache<String, Bitmap>(cacheSize){

            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() - value.getHeight();
            }

        };


    }





    public void closeActionMode() {
        if(actionMode != null) {
            actionMode.finish();
        }
    }

    public void requestUpdate() {
        notifyDataSetChanged();
    }

    public ArrayList<PInfo> getPackages() {
        return appsDetails;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener, View.OnCreateContextMenuListener  {
        ImageView icon = null;
        ImageView status = null;
        TextView name = null;
        View mainView;
        public ViewHolder(View view){
            super(view);
            mainView = view;
            icon = (ImageView)view.findViewById(R.id.app_icon);
            status = (ImageView)view.findViewById(R.id.app_status);
            status = (ImageView)view.findViewById(R.id.app_status);
            name = (TextView)view.findViewById(R.id.app_name);
            view.setOnLongClickListener(this);
            mainView.setOnClickListener(this);


        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            //menuInfo is null
            menu.add(R.string.action_settings);
            menu.add(R.string.uninstall_app);
        }


        @Override
        public void onClick(View view) {

            if(!selectionStarted) {
                PInfo info = appsDetails.get(getAdapterPosition());
                if(actionsTriggeredListener.changeAppState(info.packageName, info.appName)) {
                    notifyItemChanged(getAdapterPosition());
                }
                return;

            } else {
                appsDetails.get(getAdapterPosition()).selected = !appsDetails.get(getAdapterPosition()).selected;
                view.setSelected(appsDetails.get(getAdapterPosition()).selected);
                if (appsDetails.get(getAdapterPosition()).selected) {
                    view.setBackgroundColor(pref.getSelectionColor());
                } else {
                    view.setBackgroundColor(pref.getNormalColor());
                }
            }


        }

        @Override
        public boolean onLongClick(View view) {
            if(!selectionStarted) {
                selectionStarted = true;
                actionMode = activity.startActionMode(mActionModeCallback);
                onClick(view);
                return true;
            }
            return true;

        }


    }
    private int getAppIndex(String packageName) {
        int index = 0;
        for (PInfo info: appsDetails) {
            if(info.packageName.equals(packageName)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    public void removeApp(String packageName) {
        int pos = getAppIndex(packageName);
        if (pos >= 0) {
            notifyItemRemoved(pos);
            appsDetails.remove(pos);
        }


    }

    public void updateApp(String packageName) {
        int pos = getAppIndex(packageName);
        if (pos >= 0) {
            notifyItemChanged(pos);
            //appsDetails.remove(pos);
        }


    }


    public android.view.ActionMode.Callback mActionModeCallback = new android.view.ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {

            try {
                activity.findViewById(R.id.fab).setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            MenuInflater inflater = activity.getMenuInflater();
            inflater.inflate(R.menu.selection_menu_installed, menu);

            MenuItem menuSelectAll = menu.findItem(R.id.menu_select_all);
            menuSelectAll.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    selectAll();
                    return false;
                }
            });

            MenuItem menuShortcut = menu.findItem(R.id.menu_create_shortcut);
            menuShortcut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    actionsTriggeredListener.createShortcuts(appsDetails);
                    return false;
                }
            });

            MenuItem menuUninstall = menu.findItem(R.id.menu_uninstall);
            menuUninstall.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    //confirmAndUninstallApps();
                    actionsTriggeredListener.confirmAndUninstallApps(appsDetails);

                    return false;
                }
            });

            MenuItem menuToggle = menu.findItem(R.id.menu_toggle_state);
            menuToggle.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    return false;
                }
            });





            return true;
        }

        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {

            selectionStarted = false;
            removeAllSelection();
            activity.findViewById(R.id.fab).setVisibility(View.GONE);
            //fab.startActionMode(null);

        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            return false;
        }
    };

    private void createShortcut() {
        PackageStateChanger psc = new PackageStateChanger();
        for (PInfo pkg: appsDetails) {
            if(pkg.selected) {
                //actionsTriggeredListener.changeAppState(pkg.packageName, pkg.isEnabled);
                //addShortcut(pkg.packageName, !pkg.isEnabled, pkg.appName, pkg.iconResource);
                Log.d("PACKAGE", pkg.packageName);
            }
        }
    }



    public void removeAllSelection() {
        for(PInfo pkg: appsDetails) {
            pkg.selected = false;
        }
        notifyDataSetChanged();
    }

    private void selectAll() {
        for(PInfo pkg: appsDetails) {
            pkg.selected = true;
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_view, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        PInfo pInfo = appsDetails.get(position);
        holder.name.setText(pInfo.appName);
        final Bitmap bitmap = cache.get(pInfo.packageName);
        holder.icon.setLayoutParams(new RelativeLayout.LayoutParams(iconSize, iconSize));
        holder.icon.setScaleType(ImageView.ScaleType.CENTER_CROP);

        try {
            holder.status.setLayoutParams(new RelativeLayout.LayoutParams(iconSize, iconSize));
            holder.status.setVisibility(View.GONE);
            if (bitmap != null) {
                holder.icon.setImageBitmap(bitmap);

                BitmapManager bm = new BitmapManager(holder.icon, context, holder.status);
                bm.setPackageName(pInfo.packageName);
                bm.setStatusUpdate();
                bm.execute(position);
            } else {

                Resources resource = context.getResources();
                BitmapManager bm = new BitmapManager(holder.icon, context, holder.status);
                bm.setPackageName(pInfo.packageName);
                final BitmapManager.AsyncDrawable asyncDrawable =
                        new BitmapManager.AsyncDrawable(resource, null, bm);
                holder.icon.setImageDrawable(asyncDrawable);
                bm.execute(position);
            }
            holder.mainView.setSelected(pInfo.selected);
            if (pInfo.selected) {
                holder.mainView.setBackgroundColor(pref.getSelectionColor());
            } else {
                holder.mainView.setBackgroundColor(pref.getNormalColor());
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }




    @Override
    public int getItemCount() {
        return appsDetails.size();
    }

    public interface ActionsTriggeredListener {
        boolean changeAppState(String packageName, String appName);
        void confirmAndUninstallApps(ArrayList<PInfo> packages);
        void  createShortcuts(ArrayList<PInfo> packages);
    }

}
