package com.easwareapps.daq;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.easwareapps.daq.service.EANotificationManager;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.EALib;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class QuickChangeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String packageName = intent.getStringExtra("packageName");
        try {
            boolean newState = !new EALib().getAppsCurrentState(getApplicationContext(), packageName);

            changeAppState(packageName, newState);
            if (newState) {
                PackageManager pm = getPackageManager();
                Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
                launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
                launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(launchIntent);

                try {
                    DBHelper db = new DBHelper(getApplicationContext());
                    new EANotificationManager().showNotification(getApplicationContext(),
                            packageName,
                            new EALib().getAppName(getApplicationContext(), packageName),
                            db.getIdOfApp(packageName));
                    db.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                DBHelper db = new DBHelper(getApplicationContext());
                new EANotificationManager().clearNotification(getApplicationContext(),
                        db.getIdOfApp(packageName));
                db.close();
                Toast.makeText(getApplicationContext(),
                        getString(R.string.app_disabled,
                                new EALib().getAppName(getApplicationContext(), packageName)),
                        Toast.LENGTH_LONG)
                        .show();
            }
            DBHelper db = new DBHelper(getApplicationContext());
            db.updateRecentApp(packageName);
            db.close();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        finish();
    }

    public boolean changeAppState(String packageName, boolean currentState) {
        String newState = currentState?"enable":"disable";
        try {
            String command;
            command = "pm  " + newState + " " + packageName;
            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
            proc.waitFor();
            if(proc.exitValue() != 0) {
                BufferedReader stdError = new BufferedReader(
                        new InputStreamReader(proc.getErrorStream()));
                String s;
                String error = "";
                while ((s = stdError.readLine()) != null)
                {
                    error = s + "\n";
                }
                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();

            }else  {
                return true;
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();

        }
        return false;
    }
}
