package com.easwareapps.daq.service;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * Quoter is a Quotes collection with daily notification and widget
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.PackageStateChanger;


public class EAReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            if (intent.getAction() != null && intent.getAction().equals("disable_app")) {
                String packageName = intent.getStringExtra("package");
                Log.d("PACKAGE", packageName);
                int appId = intent.getIntExtra("app_id", -1);

                if (appId > 0) {
                    PackageStateChanger psc = new PackageStateChanger();
                    boolean result = psc.changeAppState(context, packageName, false);
                    if(result) {
                        new EANotificationManager().clearNotification(context, appId);


                    }
                    else {
                        Toast.makeText(context, "Can't Disable app", Toast.LENGTH_LONG).show();
                    }
                }
                DBHelper db = new DBHelper(context);
                db.updateRecentApp(packageName);
                db.close();

            } else if (intent.getAction() != null && intent.getAction().equals("dismiss")) {
                int appId = intent.getIntExtra("app_id", -1);
                if (appId > 0) {
                    new EANotificationManager().clearNotification(context, appId);
                }
            } else if (intent.getAction() != null && intent.getAction().equals("open_app")) {
                String packageName = intent.getStringExtra("package");
                openApp(context, packageName);
            }

        } catch (Exception e) {
            e.getLocalizedMessage();
        }


    }

    private void openApp(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
        launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launchIntent);
    }


}
