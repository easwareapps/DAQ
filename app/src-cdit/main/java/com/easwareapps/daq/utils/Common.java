package com.easwareapps.daq.utils;


/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p>
 * <p>
 * DAQ
 * Created on 10/7/17.
 */

public class Common {
    public static final String PACKAGE_NAME = "com.easwareapps.daq";
    public static final String PERSISTENT_NOTIFICATION = "persistent_notification";
    public static final String SHOW_SYSTEM_APPS = "show_system_app";
}
