package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;

import com.easwareapps.daq.R;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * EA-BulkReinstaller
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DaqPref {

    Context context;
    static DaqPref instance;

    public static final String DARK_THEME = "dark_theme";
    public static final String SHOW_SYSTEM_APP = "show_system_apps";
    public static final String BACKUP_DIR = "backup_directory";


    public static DaqPref getInstance(Context context) {

        if(instance == null) {
            instance = new DaqPref(context);
        }
        return instance;

    }
    public DaqPref(Context context) {
        this.context = context;
    }

    public void setPref(String key, String value){
        SharedPreferences pref =context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setPref(String key, boolean value){
        SharedPreferences pref =context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getPref(String key, boolean def){
        SharedPreferences pref =context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        return pref.getBoolean(key, def);
    }

    public String getPref(String key, String def){
        SharedPreferences pref =context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        return pref.getString(key, def);
    }


    public int getTheme(boolean actionbar) {
        if (getPref(DARK_THEME, false)) {
            return (actionbar)?R.style.AppTheme_Dark:R.style.AppThemeDark_NoActionBar;
        }
        return (actionbar)?R.style.AppTheme:R.style.AppTheme_NoActionBar;
    }

    public int getSelectionColor() {

        if (getPref(DARK_THEME, false)) {
            return Color.GRAY;
        }
        return Color.GRAY;
    }

    public int getNormalColor() {

        if (getPref(DARK_THEME, false)) {
            return Color.BLACK;
        }
        return Color.WHITE;
    }

    public String getBackupDir() {
        return this.getPref(BACKUP_DIR, Environment.getExternalStorageDirectory() + "/BARIA");
    }
}
