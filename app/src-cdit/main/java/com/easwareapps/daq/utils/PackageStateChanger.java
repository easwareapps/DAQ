package com.easwareapps.daq.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class PackageStateChanger {


    public boolean changeAppState(Context context, String packageName, boolean newState) {
        String strNewState = newState?"enable":"disable";
        try {
            String command;
            command = "pm  " + strNewState + " " + packageName;
            return executeCommand(command);
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public boolean executeCommand(String command) throws Exception{
        try {
            Log.e("Command", command);
            Process proc = Runtime.getRuntime().exec(new String[] { command });
            proc.waitFor();
            if(proc.exitValue() != 0) {
                BufferedReader stdError = new BufferedReader(
                        new InputStreamReader(proc.getErrorStream()));
                String s;
                String error = "";
                while ((s = stdError.readLine()) != null)
                {
                    error = s + "\n";
                }
                Log.d("Error", "Executing Command" + error);

            }else  {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getLocalizedMessage();
        }
        Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
        proc.waitFor();
        if(proc.exitValue() != 0) {
            BufferedReader stdError = new BufferedReader(
                    new InputStreamReader(proc.getErrorStream()));
            String s;
            String error = "";
            while ((s = stdError.readLine()) != null)
            {
                error = s + "\n";
            }
            Log.d("Error", "Executing Command" + error);

        }else  {
            return true;
        }
        return  false;

    }
}
