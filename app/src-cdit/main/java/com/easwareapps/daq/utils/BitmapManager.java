package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import com.easwareapps.daq.R;
import com.easwareapps.daq.adapter.InstalledAppsAdapter;

import java.lang.ref.WeakReference;

/**
 * Created by vishnu on 19/10/16.
 */
public class BitmapManager extends AsyncTask<Integer, Void, PackageDetails> {

    private final WeakReference<ImageView> imageViewReference;
    private final WeakReference<ImageView> statusImageViewReference;
    private int res = 0;
    Context context;
    int data = 0;
    String packageName;
    boolean statusUpdate = false;


    public BitmapManager(ImageView imageView, Context context, ImageView statusImageView) {
        this.context = context;
        imageViewReference = new WeakReference<>(imageView);
        statusImageViewReference = new WeakReference<>(statusImageView);
        statusUpdate = false;
    }


    public void setStatusUpdate() {
        statusUpdate = true;
    }

    public void setPackageName(String pkg){
        this.packageName = pkg;
    }



    @Override
    protected PackageDetails doInBackground(Integer... params) {
        try {
            if (statusUpdate) {
                PackageDetails pd = new PackageDetails(null, getPackageStatus(packageName));
                pd.noBitmap = true;
                return pd;
            }
        } catch (Exception e) {
            return null;
        }
        res = params[0];
        data =res;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inSampleSize = 4;
        options.inJustDecodeBounds = false;

        try {

            Bitmap bitmap = new EALib().getIcon(context, packageName);
            InstalledAppsAdapter.cache.put(packageName, bitmap);
            return new PackageDetails(bitmap, getPackageStatus(packageName));
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    private boolean getPackageStatus(String packageName) throws Exception {
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES |
                PackageManager.MATCH_DISABLED_COMPONENTS);
        return p.applicationInfo.enabled;
    }

    private Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        bmpOriginal.recycle();
        return bmpGrayscale;
    }


    @Override
    protected void onPostExecute(PackageDetails packageDetails) {
        Bitmap bitmap = null;
        boolean enabled = true;
        boolean noBitmap = false;
        if (isCancelled()) {
            packageDetails = null;
        } else {
            if(packageDetails != null) {
                bitmap = packageDetails.getBitmap();
                enabled = packageDetails.getStatus();
                noBitmap = packageDetails.noBitmap;

            }
        }

        if(noBitmap) {
            try {
                if(!enabled) {
                    //  statusImageViewReference.get().setImageResource(R.drawable.ic_disabled);
                    statusImageViewReference.get().setVisibility(View.VISIBLE);
                }
                else {
                    statusImageViewReference.get().setVisibility(View.GONE);
                }
            } catch (Exception e) {

            }
            return;
        }
        if (imageViewReference != null && (bitmap != null || noBitmap)) {
            final ImageView imageView = imageViewReference.get();
            final BitmapManager bitmapWorkerTask =
                    getBitmapManager(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                //imageView.setImageBitmap(bitmap);
                imageView.setImageBitmap(bitmap);
                try {
                    if(!enabled) {
                      //  statusImageViewReference.get().setImageResource(R.drawable.ic_disabled);
                         statusImageViewReference.get().setVisibility(View.VISIBLE);
                    }
                    else {
                        statusImageViewReference.get().setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }



            }

        }
    }





    private static BitmapManager getBitmapManager(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapManager bitmapWorkerTask = getBitmapManager(imageView);

        if (bitmapWorkerTask != null) {
            final int bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }


    public static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapManager> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapManager bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapManager>(bitmapWorkerTask);
        }

        public BitmapManager getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }
}