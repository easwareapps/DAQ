package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p>
 * <p>
 * DAQ
 * Created on 10/7/17.
 */

public class EALib {

    public Bitmap getIcon(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        Drawable icon = p.applicationInfo.loadIcon(pm);

        Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight()
                , Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        icon.draw(canvas);
        Bitmap.createScaledBitmap(bitmap, 72,72, true);
        return bitmap;
    }

    public Bitmap createCustomIcon(Bitmap pkgIcon, Bitmap daqIcon) {


        Bitmap resultBitmap = Bitmap.createBitmap(pkgIcon.getWidth(), pkgIcon.getHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(resultBitmap);

        c.drawBitmap(pkgIcon, 0, 0, null);

        Paint p = new Paint();
        p.setAlpha(127);

        c.drawBitmap(daqIcon, 0, 0, p);
        return resultBitmap;
    }

    public boolean getAppsCurrentState(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.enabled;
    }

    public String getAppName(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.loadLabel(pm).toString();
    }
}
