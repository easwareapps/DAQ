package com.easwareapps.daq.utils;

import android.graphics.Bitmap;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p>
 * <p>
 * DAQ
 * Created on 18/7/17.
 */

public class PackageDetails {

    public PackageDetails(Bitmap bitmap, boolean status) {
        this.bitmap = bitmap;
        this.status = status;
        this.noBitmap = false;
    }

    public boolean noBitmap;
    private Bitmap bitmap;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean status;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
