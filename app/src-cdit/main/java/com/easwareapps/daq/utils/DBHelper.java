package com.easwareapps.daq.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "daq.db";
    private static final int DB_VERSION = 1;

    private static final String TABLE_APPS = "apps_details";
    private static final String TABLE_USAGE = "apps_usage";

    private static final String KEY_ID = "_id";
    private static final String KEY_APP_NAME = "app_name";
    private static final String KEY_PACKAGE_NAME = "package_name";
    private static final String KEY_SYSTEM_APP = "system_app";

    private static final String KEY_USAGE_TIME = "usage_time";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            String query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, " +
                    "%s VARCHAR(200), %s VARCHAR(500), %s int)", TABLE_APPS,
                    KEY_ID, KEY_APP_NAME, KEY_PACKAGE_NAME, KEY_SYSTEM_APP);
            Log.d("QUERY", query);
            sqLiteDatabase.execSQL(query);

            query = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, " +
                            "%s VARCHAR(200), %s int)", TABLE_USAGE, KEY_ID, KEY_PACKAGE_NAME,
                    KEY_USAGE_TIME);
            Log.d("QUERY", query);
            sqLiteDatabase.execSQL(query);
        } catch (Exception e) {
            e.getLocalizedMessage();

        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public ArrayList<PInfo> getAllApps(String s, boolean showSystemApp) {
        String columns[] = {KEY_APP_NAME, KEY_PACKAGE_NAME, KEY_SYSTEM_APP};
        String whereArgs[] = null;
        String where = "";
        if(!showSystemApp) {
            where = KEY_SYSTEM_APP + "=?";
            whereArgs = new String[]{"0"};
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        if(s.equals("")) {
            cursor = db.query(TABLE_APPS, columns, where, whereArgs, null, null,
                    KEY_APP_NAME + " ASC");
        } else {
            if(whereArgs == null || whereArgs.length == 0) {
                whereArgs = new String[]{"%" + s+ "%"};
                where += KEY_APP_NAME + " LIKE ?";
            } else {
                whereArgs = new String[]{"0", "%" + s+ "%"};
                where += " AND " +KEY_APP_NAME + " LIKE ? ";
            }
            Log.d("WERE", where);

            cursor = db.query(TABLE_APPS, columns, where, whereArgs, null, null,
                    KEY_APP_NAME + " ASC");
        }
        ArrayList<PInfo> infos = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                PInfo pInfo = new PInfo();
                pInfo.appName = cursor.getString(0);
                pInfo.packageName = cursor.getString(1);
                pInfo.isSystemApp = (cursor.getInt(2)==1);
                infos.add(pInfo);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e){
            e.getLocalizedMessage();
        }
        try {
            cursor.close();
        } catch (Exception e){
            e.getLocalizedMessage();
        }
        return infos;
    }

    public ArrayList<PInfo> getRecentApps(String s, boolean showSystemApp) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String query = "SELECT app." + KEY_APP_NAME + ", app." + KEY_PACKAGE_NAME +
                ", app." + KEY_SYSTEM_APP + " FROM " + TABLE_APPS + " app INNER JOIN " +
                TABLE_USAGE + " usage ON app." + KEY_PACKAGE_NAME + " = usage." +
                KEY_PACKAGE_NAME + " WHERE usage." + KEY_USAGE_TIME + " > 0 ";
        if (!showSystemApp) {
            query += " and app." + KEY_SYSTEM_APP + " = 0";
        }
        String whereArgs[] = null;
        if (!s.equals("")) {
            query += " and app." + KEY_APP_NAME + " LIKE ? ";
            whereArgs = new String[]{"%" + s + "%"};

        }
        query += " ORDER BY usage." +
                KEY_USAGE_TIME + " DESC";
        Log.d("QUERY", query);
        cursor = db.rawQuery(query, whereArgs);


        ArrayList<PInfo> infos = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                PInfo pInfo = new PInfo();
                pInfo.appName = cursor.getString(0);
                pInfo.packageName = cursor.getString(1);
                pInfo.isSystemApp = (cursor.getInt(2)==1);
                infos.add(pInfo);
            } while (cursor.moveToNext());
        }
        try {
            cursor.close();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        db.close();
        return infos;
    }

    public void addAppsToDB(ArrayList<PInfo> infos) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_APPS);
        ContentValues cv = new ContentValues();
        for (PInfo pinfo: infos) {
            cv.put(KEY_APP_NAME, pinfo.appName);
            cv.put(KEY_PACKAGE_NAME, pinfo.packageName);
            cv.put(KEY_SYSTEM_APP, pinfo.isSystemApp?1:0);
            db.insert(TABLE_APPS, null, cv);
        }

    }

    public void removeApp(String packageName) {
        Log.d("REMOVING", packageName);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_APPS + " WHERE " + KEY_PACKAGE_NAME + " = ?",
                new String[] {packageName});
        db.close();
        Log.d("REMOVED", packageName);
    }

    public void addApp(PInfo pinfo) {
        Log.d("ADDING", pinfo.packageName);
        removeApp(pinfo.packageName);
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_APP_NAME, pinfo.appName);
        cv.put(KEY_PACKAGE_NAME, pinfo.packageName);
        cv.put(KEY_SYSTEM_APP, pinfo.isSystemApp ? 1 : 0);
        db.insert(TABLE_APPS, null, cv);
        db.close();
        Log.d("ADDED", pinfo.packageName);

    }

    private int getIdInRecentApp(String packageName) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USAGE, new String[]{KEY_ID}, KEY_PACKAGE_NAME + "=?",
                new String[]{packageName}, null, null, null);
        int id = 0;
        if(cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return id;
    }

    public int getIdOfApp(String packageName) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_APPS, new String[]{KEY_ID}, KEY_PACKAGE_NAME + "=?",
                new String[]{packageName}, null, null, null);
        int id = 0;
        if(cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }
        try {
            cursor.close();
            db.close();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        return id;
    }

    public void updateRecentApp(String packageName) {
        int id = getIdInRecentApp(packageName);
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(KEY_PACKAGE_NAME, packageName);
        cv.put(KEY_USAGE_TIME, System.currentTimeMillis());
        if(id > 0) {
            db.update(TABLE_USAGE, cv, KEY_ID + "=?", new String[]{String.valueOf(id)});
        } else {
            db.insert(TABLE_USAGE, null, cv);
        }
        try {
            db.close();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }
}
