//package com.easwareapps.daq;
//
//
//import android.os.Bundle;
//import android.provider.Settings;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.DisplayMetrics;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Toast;
//
//import com.easwareapps.daq.utils.DaqPref;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.NativeExpressAdView;
//
//import io.fabric.sdk.android.services.common.CommonUtils;


//public class SettingsActivity extends AppCompatActivity {
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setTheme(DaqPref.getInstance(getApplicationContext()).getTheme(true));
//        setContentView(R.layout.settings_layout);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        if( toolbar != null) {
//            setSupportActionBar(toolbar);
//        }
//
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == android.R.id.home) {
//            finish();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onBackPressed() {
//        finish();
//        super.onBackPressed();
//    }
//
//    public void refer(View v) {
//
//        Toast.makeText(getApplicationContext(), "REFER on SA", Toast.LENGTH_LONG).show();
//
//    }
//
//    public void gotoPlayStore(View v) {
//        Toast.makeText(getApplicationContext(), "Ps on SA", Toast.LENGTH_LONG).show();
//    }
//}


package com.easwareapps.daq;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import com.easwareapps.daq.utils.DaqPref;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DaqPref pref = DaqPref.getInstance(getApplicationContext());
        int theme = pref.getTheme(false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        setSupportActionBar(toolbar);
        if(toolbar != null) {
            toolbar.setTitle(R.string.settings);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }




        Switch switchDarkTheme = (Switch) findViewById(R.id.dark_theme);
        boolean value = pref.getPref(getString(R.string.key_enable_dark_theme), false);
        switchDarkTheme.setChecked(value);
        int themeDescription = value?R.string.dark_theme_enabled: R.string.dark_theme_disabled;
        ((TextView)findViewById(R.id.description_theme)).setText(themeDescription);
        switchDarkTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                DaqPref pref = DaqPref.getInstance(getApplicationContext());
                pref.setPref(getString(R.string.key_enable_dark_theme), b);
                SettingsActivity.this.recreate();
            }
        });

        Switch switchSystemApps = (Switch) findViewById(R.id.show_system_apps);
        value = pref.getPref(getString(R.string.key_show_system_app), false);
        int systemAppDesc = value?R.string.system_apps_are_hidden: R.string.system_apps_are_shown;
        ((TextView)findViewById(R.id.description_system_apps)).setText(systemAppDesc);
        switchSystemApps.setChecked(value);
        switchSystemApps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                DaqPref pref = DaqPref.getInstance(getApplicationContext());
                pref.setPref(getString(R.string.key_show_system_app), b);
                int systemAppDesc = b?R.string.system_apps_are_hidden: R.string.system_apps_are_shown;
                ((TextView)findViewById(R.id.description_system_apps)).setText(systemAppDesc);
            }
        });


        updateDescription();





    }



    private void updateDescription() {
        int pos = DaqPref.getInstance(getApplicationContext()).getPref(getString(R.string.key_shortcut_action), 0);
        String defActions = getResources().getStringArray(R.array.shortcut_actions)[pos];
        ((TextView)findViewById(R.id.description_shortcuts)).setText(defActions);

        pos = DaqPref.getInstance(getApplicationContext()).getPref(getString(R.string.key_show_notification), 0);
        String defNotification = getResources().getStringArray(R.array.notification_types)[pos];
        ((TextView)findViewById(R.id.description_notifications)).setText(defNotification);
    }


    public void gotoPlayStore(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.easwareapps.daq"));
        startActivity(intent);
    }




    public void refer (View v) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        share.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.daq));
        share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.daq_download,
                "https://play.google.com/store/apps/details?id=com.easwareapps.daq"));
        startActivity(share);

    }

    public void chooseShortcutAction (View v) {
        int pos = DaqPref.getInstance(getApplicationContext()).getPref(getString(R.string.key_shortcut_action), 0);
        String[] actions = getResources().getStringArray(R.array.shortcut_actions);
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setSingleChoiceItems(actions, pos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DaqPref.getInstance(getApplicationContext()).setPref(getString(R.string.key_shortcut_action), i);
                updateDescription();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                updateDescription();
            }
        }).setPositiveButton(R.string.done, null).show();
    }

    public void chooseNotifications (View v) {
        int pos = DaqPref.getInstance(getApplicationContext()).getPref(getString(R.string.key_show_notification), 0);
        String[] actions = getResources().getStringArray(R.array.notification_types);
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setSingleChoiceItems(actions, pos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DaqPref.getInstance(getApplicationContext()).setPref(getString(R.string.key_show_notification), i);
                updateDescription();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                updateDescription();
            }
        }).setPositiveButton(R.string.done, null).show();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }





}
