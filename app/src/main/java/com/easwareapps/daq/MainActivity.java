package com.easwareapps.daq;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.easwareapps.daq.adapter.InstalledAppsAdapter;
import com.easwareapps.daq.fragments.InstalledAppsFragment;
import com.easwareapps.daq.utils.DaqPref;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.EALib;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.utils.PackageStateChanger;
import com.getbase.floatingactionbutton.FloatingActionsMenu;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



public class MainActivity extends AppCompatActivity
        implements InstalledAppsAdapter.ActionsTriggeredListener,
        SharedPreferences.OnSharedPreferenceChangeListener,
        PackageStateChanger.CommandFinishedListener {


    private static final int SETTINGS_REQ = 1000;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    public static boolean showSystemApp = false;
    public static final int ALL_APPS = 0;
    public static final int RECENT_APPS = 1;
    public static final int DISABLED_APPS = 2;


    InstalledAppsFragment allAppsFragment = null;
    InstalledAppsFragment recentAppsFragment = null;

    private int theme;
    private  SearchView searchView;

    ActionMode actionMode;
    private MenuItem menuToggleSystemApps = null;
    private PackageManager pm;








    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        DaqPref pref = DaqPref.getInstance(getApplicationContext());
        theme = pref.getTheme(false);
        showSystemApp = pref.getPref(getString(R.string.key_show_system_app), false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        if(savedInstanceState != null) {
//            savedInstanceState.ge
//        }
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar != null)
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                try {
                    if(actionMode != null) {
                        if(allAppsFragment.getCurrentAdapter().selectionStarted) {
                            allAppsFragment.getCurrentAdapter().selectionStarted = false;
                            allAppsFragment.removeAllSelection();
                            closeActionMode();
                        } else if(recentAppsFragment.getCurrentAdapter().selectionStarted) {
                            recentAppsFragment.getCurrentAdapter().selectionStarted = false;
                            recentAppsFragment.removeAllSelection();
                            closeActionMode();
                        }


                    }
                } catch (Exception e) {
                    e.getLocalizedMessage();

                }
                try {
                    if(getCurrentFragment().isSearchViewExpanded) {
                        searchMenuItem.expandActionView();
                        searchView.setQuery(getCurrentFragment().searchText, true);
                    } else {
                        searchMenuItem.collapseActionView();
                    }
                } catch (Exception e) {
                    e.getLocalizedMessage();
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        SharedPreferences preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(this);



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);



        preferences.registerOnSharedPreferenceChangeListener(this);

        if(!new EALib().isRooted()) {
            Snackbar.make(findViewById(R.id.main_content),
                    R.string.not_rooted, Snackbar.LENGTH_LONG)
                    .setAction(R.string.info, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse("https://goo.gl/J5Utsb"));
                            startActivity(i);
                        }
                    }).show();
        }















    }




    MenuItem searchMenuItem;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menuToggleSystemApps = menu.findItem(R.id.action_toggle_system_app_visibility);
        changeMenuText();
        searchMenuItem = menu.findItem(R.id.search);
        if(searchView == null)
            searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!getCurrentFragment().searchText.equals(newText)) {
                    getCurrentFragment().searchText = newText;
                    getCurrentFragment().reloadApps(newText, false);
                }
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                getCurrentFragment().isSearchViewExpanded = false;
                return true;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchMenuItem,
                new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                try {
                    getCurrentFragment().isSearchViewExpanded = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                try {
                    getCurrentFragment().isSearchViewExpanded = false;
                } catch (Exception e) {
                        e.printStackTrace();
                    }
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent settings = new Intent(this, SettingsActivity.class);
            startActivityForResult(settings, SETTINGS_REQ);
            return true;
        }

        else if (id == R.id.action_toggle_system_app_visibility) {
            showSystemApp = !showSystemApp;
            if(getCurrentFragment() != null) {
                getCurrentFragment().reloadApps("", false);
            }
            changeMenuText();

        }
//        else if (id == R.id.action_refresh) {
//            if (mViewPager.getCurrentItem() == 0) {
//                getCurrentFragment().reloadApps("", true);
//            }
//        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        preferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_REQ) {

            DaqPref pref = DaqPref.getInstance(getApplicationContext());
            if (showSystemApp != pref.getPref(getString(R.string.key_show_system_app), false)) {
                showSystemApp = pref.getPref(getString(R.string.key_show_system_app), false);
                try {
                    allAppsFragment.reloadApps(allAppsFragment.searchText, false);
                } catch (Exception e) {

                }

                try {
                    recentAppsFragment.reloadApps(recentAppsFragment.searchText, false);
                } catch (Exception e) {

                }

            }

            if (DaqPref.getInstance(getApplicationContext()).getTheme(false) != theme) {
                finish();
                startActivity(new Intent(MainActivity.this, MainActivity.class));

            }


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void changeMenuText() {
        if(menuToggleSystemApps != null) {
            if (showSystemApp) {
                menuToggleSystemApps.setTitle(R.string.hide_system_apps);
            } else {
                menuToggleSystemApps.setTitle(R.string.show_system_apps);
            }
        }
    }

    @Override
    public void toggleAppState(String packageName, String appName) {
        changeAppState(packageName, appName, true);
    }


    private void changeAppState(String packageName, String appName, boolean openApp) {
        if(packageName.equals(getPackageName())) {
            Snackbar.make(findViewById(R.id.main_content),
                    R.string.cant_disable_daq_itself, Snackbar.LENGTH_LONG).show();
            return;
        }
        boolean newState = true;
        try {
            PackageManager pm = getPackageManager();
            PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            newState = !p.applicationInfo.enabled;
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        PackageStateChanger psc = new PackageStateChanger(MainActivity.this, MainActivity.this,
                newState && openApp);
        PInfo info = new PInfo();
        info.packageName = packageName;
        info.isEnabled = !newState;
        info.appName = appName;
        psc.execute(info);

    }

    AlertDialog ad;
    @Override
    public void confirmAndUninstallApps(final ArrayList<PInfo> packages) {
        int count = 0;
        int noSysApps =0;
        for(PInfo pInfo:packages) {
            if (pInfo.selected) {
               count++;
                if(pInfo.isSystemApp) noSysApps++;
            }
        }
        Log.d("SYSAPPS=", String.valueOf(noSysApps));
        String extraMsg = "";
        if(noSysApps > 0) {
            extraMsg = getString(R.string.contains_system_app, noSysApps);
        }
        if(count == 0 )return;
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
        adb.setTitle(R.string.uninstall_app);
        adb.setMessage(String.format(getString(R.string.desc_uninstall_app), count)
                + extraMsg);
        adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ad.dismiss();
                uninstallPackages(packages);
            }
        });
        adb.setNegativeButton(R.string.cancel, null);
        ad = adb.show();
    }

    @Override
    public void createShortcuts(ArrayList<PInfo> packages) {

        for(PInfo pInfo:packages) {
            if(!pInfo.selected) continue;
            try {
                String packageName = pInfo.packageName;
                String title = pInfo.appName;
                Intent shortcutIntent = new Intent(getApplicationContext(),
                        QuickChangeActivity.class);
                shortcutIntent.putExtra("packageName", packageName);
                //shortcutIntent.putExtra("newState", newState);
                shortcutIntent.setAction(Intent.ACTION_MAIN);

                Intent addIntent = new Intent();
                addIntent
                        .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, title);
                Bitmap bmp = new EALib().getIcon(getApplicationContext(), packageName, -1, -1);
                Bitmap bmp1 = new EALib().getIcon(getApplicationContext(),
                        "com.easwareapps.daq", bmp.getWidth()/3, bmp.getHeight()/3);

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(
                        new EALib().createCustomIcon(bmp, bmp1), 72, 72, true);

                addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, scaledBitmap);

                addIntent
                        .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                getApplicationContext().sendBroadcast(addIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }



    @Override
    public void changeActionMode(boolean state) {
        findViewById(R.id.fab).setVisibility(state?View.VISIBLE:View.GONE);
        actionMode = startActionMode(mActionModeCallback);
    }

    private void uninstallPackages(ArrayList<PInfo> packages) {
        final ProgressDialog pd = new ProgressDialog(MainActivity.this);
        pd.setTitle("Uninstalling Apps");
        pd.setMessage("Please wait...");
        pd.show();

        try {
            //PackageStateChanger psc = new PackageStateChanger();
            int count = 0;
            for (PInfo pInfo : packages) {
                if(pInfo.packageName.equals(getPackageName())) {
                    Snackbar.make(findViewById(R.id.main_content),
                            R.string.cant_uninstall_daq_itself, Snackbar.LENGTH_LONG).show();
                    continue;
                }
                if (pInfo.selected && pInfo.packageName!=getPackageName()) {
                    count++;
                }
            }
            PInfo infos[] = new PInfo[count];
            int index = 0;
            for (PInfo pInfo : packages) {
                if (pInfo.selected) {
                    if (!pInfo.packageName.equals(getPackageName())) {
                        infos[index++] = pInfo;
                    }
                }
            }
            PackageStateChanger psc = new PackageStateChanger(MainActivity.this,
                    MainActivity.this);
            psc.execute(infos);
        } catch (Exception e) {
            e.printStackTrace();
        }
        pd.dismiss();
        closeActionMode();
    }

    @Override
    public void toggleApps(ArrayList<PInfo> packages) {
        PackageStateChanger psc = new PackageStateChanger(MainActivity.this,
                MainActivity.this, false);
        try {
            int index = 0;
            int count = 0;
            for(PInfo info: packages) {
                if(info.selected)
                    count++;
            }
            PInfo[] infos = new PInfo[count];
            for(PInfo info: packages) {
                if(info.selected)
                    infos[index++] = info;
            }
            psc.execute(infos);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public void update(String packageName) {
        if(allAppsFragment != null) {
            try {
                allAppsFragment.update(packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } if(recentAppsFragment != null) {
            try {
                recentAppsFragment.update(packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public InstalledAppsFragment getCurrentFragment() {
        if(mViewPager.getCurrentItem() == RECENT_APPS) {
            return recentAppsFragment;
        } else if (mViewPager.getCurrentItem() == ALL_APPS) {
            return allAppsFragment;
        }
        return null;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

//        if(key == getString(R.string.key_enable_dark_theme)) {
////            MainActivity.this.recreate();
//            finish();
//            startActivity(new Intent(MainActivity.this, MainActivity.class));
//        }
//        else
        if(key == getString(R.string.key_show_system_app)) {
            String searchText = allAppsFragment.searchText == null?"":allAppsFragment.searchText;
            allAppsFragment.reloadApps(searchText,
                    sharedPreferences.getBoolean(getString(R.string.key_show_system_app),
                            getResources().getBoolean(R.bool.def_show_system_apps)));
            searchText = recentAppsFragment.searchText == null?"":recentAppsFragment.searchText;
            recentAppsFragment.reloadApps(searchText,
                    sharedPreferences.getBoolean(getString(R.string.key_show_system_app),
                            getResources().getBoolean(R.bool.def_show_system_apps)));
        }

    }

    @Override
    public void onCommandFinished(boolean result, String packageName, boolean shouldOpen) {

        if(!result) {
            Snackbar.make(findViewById(R.id.main_content),
                    R.string.permission_denied, Snackbar.LENGTH_LONG).show();
            return;
        }


        DBHelper db = new DBHelper(getApplicationContext());
        db.updateRecentApp(packageName);
        db.close();
        try {
            if (recentAppsFragment != null) {
                recentAppsFragment.update(packageName);
            }
            if (allAppsFragment != null) {
                allAppsFragment.update(packageName);
            }
        } catch (Exception e) {

        }

        if(shouldOpen) {
            openApp(packageName);
        }

    }

    @Override
    public void onUninstallFinished(boolean status, String packageName) {
        if(!status) {
            Snackbar.make(findViewById(R.id.main_content),
                    R.string.permission_denied, Snackbar.LENGTH_LONG).show();
            return;
        }
        try {
            allAppsFragment.removeApp(packageName);
            recentAppsFragment.removeApp(packageName);
        } catch (Exception e) {

        }
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    if(allAppsFragment == null) {
                        allAppsFragment = InstalledAppsFragment.getInstance(MainActivity.this,
                                MainActivity.ALL_APPS);
                        allAppsFragment.setRetainInstance(true);
                    }

                    return allAppsFragment;
                case 1:

                    if(recentAppsFragment == null) {
                        recentAppsFragment = InstalledAppsFragment.getInstance(MainActivity.this,
                                MainActivity.RECENT_APPS);
                        recentAppsFragment.setRetainInstance(true);
                    }

                    return recentAppsFragment;

            }

            return null;


        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.all);
                case 1:
                    return getResources().getString(R.string.recent);
                case 2:
                    return getResources().getString(R.string.disabled);
            }
            return null;
        }


    }

    @Override
    public void onBackPressed() {
        try {
            getSupportFragmentManager().beginTransaction().detach(allAppsFragment).commit();
            getSupportFragmentManager().beginTransaction().detach(recentAppsFragment).commit();

        } catch (Exception e) {

        }

        super.onBackPressed();
    }


    public android.view.ActionMode.Callback mActionModeCallback = new android.view.ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {

            getCurrentFragment().getCurrentAdapter().selectionStarted = true;
            try {
                findViewById(R.id.fab).setVisibility(View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.selection_menu_installed, menu);

            MenuItem menuSelectAll = menu.findItem(R.id.menu_select_all);
            menuSelectAll.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getCurrentFragment().selectAll();
                    return false;
                }
            });

            MenuItem menuShortcut = menu.findItem(R.id.menu_create_shortcut);
            menuShortcut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getCurrentFragment().createShortcuts();
                    return false;
                }
            });

            MenuItem menuUninstall = menu.findItem(R.id.menu_uninstall);
            menuUninstall.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    getCurrentFragment().confirmAndUninstallApps();
                    return false;
                }
            });

            MenuItem menuToggle = menu.findItem(R.id.menu_toggle_state);
            menuToggle.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    return false;
                }
            });





            return true;
        }

        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {

            getCurrentFragment().removeAllSelection();
            findViewById(R.id.fab).setVisibility(View.GONE);
            FloatingActionsMenu fam = ( FloatingActionsMenu)  findViewById(R.id.fab);
            fam.collapse();
            getCurrentFragment().getCurrentAdapter().selectionStarted = false;
            //fab.startActionMode(null);

        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            return false;
        }
    };

    public void closeActionMode() {
        if(actionMode != null) {
            actionMode.finish();
        }
    }

    public void createShortcutForSelectedPackages(View v){
        FloatingActionsMenu fam = ( FloatingActionsMenu)  findViewById(R.id.fab);
        fam.collapse();
        getCurrentFragment().createShortcuts();
        closeActionMode();
    }

    public void uninstallSelectedPackages(View v){
        FloatingActionsMenu fam = ( FloatingActionsMenu)  findViewById(R.id.fab);
        fam.collapse();
        getCurrentFragment().confirmAndUninstallApps();
        //closeActionMode();
    }

    public void toggleStateOfSelectedPackages(View v){
        FloatingActionsMenu fam = ( FloatingActionsMenu)  findViewById(R.id.fab);
        fam.collapse();
        getCurrentFragment().toggleApps();
        closeActionMode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            try {
                if(allAppsFragment != null && allAppsFragment.getCurrentAdapter() != null)
                    allAppsFragment.getCurrentAdapter().setTriggerListener(MainActivity.this);

            } catch (Exception e) {

            }

            try {
                if(recentAppsFragment != null && recentAppsFragment.getCurrentAdapter() != null)
                    recentAppsFragment.getCurrentAdapter().setTriggerListener(MainActivity.this);
            } catch (Exception e) {

            }
            new AppsDataChecker().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        try {
            allAppsFragment.setNoColumns();
            allAppsFragment.reloadApps(allAppsFragment.searchText, false);

            recentAppsFragment.setNoColumns();
            recentAppsFragment.reloadApps(recentAppsFragment.searchText, false);

        } catch (Exception e) {

        }

        super.onConfigurationChanged(newConfig);

    }

    private void openApp(String packageName) {
        try {

            PackageManager pm = getPackageManager();
            Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
            launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
            launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(launchIntent);

        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    class AppsDataChecker extends AsyncTask<Void, String, Boolean> {

        //boolean shouldUpdateDB;
        @Override
        protected Boolean doInBackground(Void... voids) {
            if (allAppsFragment != null) {
                DBHelper dbHelper = new DBHelper(getApplicationContext());
                String text = allAppsFragment.searchText==null?"":allAppsFragment.searchText;
                ArrayList<PInfo> appsFromDB = dbHelper.getAllApps(text, showSystemApp);
                if (appsFromDB.size() == 0) {
                    dbHelper.close();
                    return false;
                } else if (allAppsFragment != null) {
                    if (allAppsFragment.getAppsList().size() > 0 &&
                            shouldUpdate(appsFromDB, allAppsFragment.getAppsList())) {
                        dbHelper.close();
                        return true;
                    }

                }
                appsFromDB = dbHelper.getAllApps(text, showSystemApp);
                ArrayList<PInfo> systemApps = getAppsFromSystem();
                boolean result = shouldUpdate(appsFromDB, systemApps);
                if (result ) {
                    dbHelper.addAppsToDB(systemApps);
                    dbHelper.close();
                    return result;
                }

            }
            return false;
        }

        @Override
        protected void onProgressUpdate(String... packageNames) {
            if(packageNames == null || packageNames.length ==0) {
                allAppsFragment.reloadApps(allAppsFragment.searchText, false);
                recentAppsFragment.reloadApps(recentAppsFragment.searchText, false);
            }
            for(String packageName:packageNames) {
                try {
                    allAppsFragment.update(packageName);
                    recentAppsFragment.update(packageName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            try {
                if (aBoolean) {
                    if (allAppsFragment != null && !allAppsFragment.getCurrentAdapter().selectionStarted) {
                        //float pos = allAppsFragment.getScrollPosition();
                        allAppsFragment.reloadApps(allAppsFragment.searchText, false);
                       // allAppsFragment.setScrollPosition(pos);
                    }
                    if (recentAppsFragment != null &&
                            !recentAppsFragment.getCurrentAdapter().selectionStarted) {
                       // float pos = recentAppsFragment.getScrollPosition();
                        recentAppsFragment.reloadApps(recentAppsFragment.searchText, false);
                       // recentAppsFragment.setScrollPosition(pos);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private ArrayList<PInfo> getAppsFromSystem() {

            if(pm == null) {
                pm = getPackageManager();
            }
            ArrayList<PInfo> result = new ArrayList<>();
            int flags = PackageManager.GET_META_DATA |
                    PackageManager.GET_SHARED_LIBRARY_FILES |
                    PackageManager.GET_GIDS;
            List<PackageInfo> packs;

            packs = pm.getInstalledPackages(flags);


            for (PackageInfo p : packs) {
                try {
                    PInfo newInfo = new PInfo();
                    newInfo.appName = p.applicationInfo.loadLabel(pm).toString();
                    newInfo.packageName = p.packageName;
                    ApplicationInfo ai = pm.getApplicationInfo(p.packageName, 0);
                    newInfo.isSystemApp = ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
                    if(!showSystemApp && newInfo.isSystemApp) {
                        continue;
                    }
                    newInfo.isEnabled = getPackageStatus(p.packageName);
                    result.add(newInfo);

                } catch (Exception e) {

                    Log.d("Package Exception", e.getLocalizedMessage());
                }
            }
            Collections.sort(result, new Comparator<PInfo>() {

                public int compare(PInfo p1, PInfo p2) {
                    return p1.appName.compareToIgnoreCase(p2.appName);
                }
            });

            return result;
        }

        private boolean getPackageStatus(String packageName) throws Exception {
            if(pm == null) {
                pm = getPackageManager();
            }
            PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return p.applicationInfo.enabled;
        }

        private boolean shouldUpdate(ArrayList<PInfo> appsFromDB, ArrayList<PInfo> set2) {
            try {
                if (appsFromDB == null) {
                    return false;
                }
                if (appsFromDB.size() != set2.size()) {
                    return true;
                }
                for (int i = 0; i < appsFromDB.size(); i++) {
                    if (!appsFromDB.get(i).packageName.equals(set2.get(i).packageName)) {
                        return true;
                    } else if (appsFromDB.get(i).isEnabled != set2.get(i).isEnabled) {
                        publishProgress(appsFromDB.get(i).packageName);
                    }
                }
                return false;
            } catch (Exception e) {
                return true;
            }
        }
    }

    @Override
    protected void onPause() {
        try {
            if(allAppsFragment != null && allAppsFragment.getCurrentAdapter() != null)
                allAppsFragment.getCurrentAdapter().setTriggerListener(null);

        } catch (Exception e) {

        }

        try {
            if(recentAppsFragment != null && recentAppsFragment.getCurrentAdapter() != null)
                recentAppsFragment.getCurrentAdapter().setTriggerListener(null);
        } catch (Exception e) {

        }
        super.onPause();
    }


}
