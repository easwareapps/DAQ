package com.easwareapps.daq.utils;

import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.app.AlertDialog;
import android.util.Log;

import com.easwareapps.daq.DAQWidget;
import com.easwareapps.daq.R;
import com.easwareapps.daq.service.EANotificationManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class PackageStateChanger extends AsyncTask<PInfo, PInfo, PInfo>{

    private CommandFinishedListener listener;
    private boolean shouldOpen;
    private ArrayList<PInfo> infos = new ArrayList<>();
    private int type;
    private static final int CHANGE_STATE = 100;
    private static final int UNINSTALL = 101;
    private Context context;
    AlertDialog pd;

    public PackageStateChanger(Context context, CommandFinishedListener listener,
                               boolean shouldOpen) {
        this.context = context;
        this.listener = listener;
        this.shouldOpen = shouldOpen;
        this.type = CHANGE_STATE;
    }

    public PackageStateChanger(Context context, CommandFinishedListener listener) {
        this.context = context;
        this.listener = listener;
        this.type = UNINSTALL;
    }


    @Override
    protected void onPreExecute() {
        try {
            pd = new ProgressDialog(context);
            if (type == UNINSTALL)
                pd.setMessage(context.getString(R.string.uninstalling_please_wait));
            else if (type == CHANGE_STATE)
                pd.setMessage(context.getString(R.string.please_wait));
            pd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPreExecute();
    }


    @Override
    protected PInfo doInBackground(PInfo... infos) {
        for(PInfo info: infos) {
            try {
                PInfo pinfo = info;
                if(type == UNINSTALL) {
                    if(executeCommand("pm uninstall " + info.packageName)) {
                        pinfo.comandExecuted = true;
                    } else {
                        pinfo.comandExecuted = false;
                    }
                } else if(type == CHANGE_STATE) {

                    if (changeAppState(info.packageName, !info.isEnabled)) {

                        pinfo.isEnabled = !info.isEnabled;
                        pinfo.comandExecuted = true;
                    } else {
                        pinfo.comandExecuted = false;
                    }
                }
                publishProgress(pinfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(shouldOpen && infos.length == 1)
            return infos[0];
        return null;
    }

    @Override
    protected void onProgressUpdate(PInfo... values) {
        try {
            if(type == CHANGE_STATE) {
                listener.onCommandFinished(values[0].comandExecuted, values[0].packageName, shouldOpen);
            } else if(type == UNINSTALL) {
                listener.onUninstallFinished(values[0].comandExecuted, values[0].packageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(PInfo aVoid) {
        try {
            if (pd != null)
                pd.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Intent i = new Intent(context, DAQWidget.class);
            i.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            int ids[] = AppWidgetManager.getInstance(context).getAppWidgetIds
                    (new ComponentName(context, DAQWidget.class));
            i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
            context.sendBroadcast(i);




        } catch (Exception e) {
            e.printStackTrace();

        }
        //TODO
        if(shouldOpen) {
            try {
                openApp(context, aVoid.packageName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPostExecute(aVoid);
    }

    private void openApp(Context context, String packageName) throws Exception {
        PackageManager pm = context.getPackageManager();
        Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
        launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launchIntent);
    }

    public boolean changeAppState(String packageName, boolean newState) {
        String strNewState = newState?"enable":"disable";
        try {
            String command;
            command = "pm  " + strNewState + " " + packageName;
            boolean result = executeCommand(command);
            if(result) {
                DBHelper dbHelper = new DBHelper(context);
                dbHelper.updateAppStatus(packageName, newState);
                dbHelper.updateRecentApp(packageName);

                showNotificationIfNeeded(context, newState, packageName, dbHelper.getAppName(packageName));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public boolean executeCommand(String command) throws Exception{

        Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
        proc.waitFor();
        if(proc.exitValue() != 0) {
            BufferedReader stdError = new BufferedReader(
                    new InputStreamReader(proc.getErrorStream()));
            String s;
            String error = "";
            while ((s = stdError.readLine()) != null)
            {
                error = s + "\n";
            }
            Log.d("Error", "Executing Command" + error);

        }else  {
            return true;
        }
        return  false;

    }

    private void showNotificationIfNeeded(Context context, boolean newState,
                                          String packageName, String appName) {
        boolean notificationNeeded = DaqPref.getInstance(context)
                .shouldShowNotification(newState);

        DBHelper db = new DBHelper(context);
        int nid = db.getIdOfApp(packageName);
        new EANotificationManager().clearNotification(context, nid);
        db.close();
        if (notificationNeeded) {
            try {
                new EANotificationManager().showNotifications(context,
                        packageName, appName, nid, newState);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface CommandFinishedListener {
        void onCommandFinished(boolean status, String packageName, boolean shouldOpen);
        void onUninstallFinished(boolean status, String packageName);
    }
}
