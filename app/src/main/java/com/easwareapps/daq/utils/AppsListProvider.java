package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.easwareapps.daq.R;
import com.easwareapps.daq.adapter.InstalledAppsAdapter;

import java.util.ArrayList;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AppsListProvider implements RemoteViewsService.RemoteViewsFactory {


    Context context = null;
    private ArrayList<PInfo> apps = new ArrayList<>();
    int appWidgetId;

    public AppsListProvider(Context context, int app) {
        DBHelper dbHelper = new DBHelper(context);
        this.context = context;
        apps = dbHelper.getRecentApps("", true);
        dbHelper.close();
    }

    @Override
    public int getCount() {
        Log.d("COUNT", apps.size() + " !");
        return apps.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public RemoteViews getViewAt(int position) {


        final RemoteViews remoteView = new RemoteViews(
                "com.easwareapps.daq", R.layout.widget_app_layout);

        try {

            remoteView.setTextViewText(R.id.app_name, apps.get(position).appName);
            if(InstalledAppsAdapter.cache == null) {
                final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
                int cacheSize = maxMemory/8;
                InstalledAppsAdapter.cache = new LruCache<String, Bitmap>(cacheSize){

                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getRowBytes() - value.getHeight();
                    }

                };
            }
            Bitmap bitmap = InstalledAppsAdapter.cache.get(apps.get(position).packageName);
            if (bitmap == null) {
                bitmap = new EALib().getIcon(context, apps.get(position).packageName, -1, -1);
                InstalledAppsAdapter.cache.put(apps.get(position).packageName, bitmap);

            }
            remoteView.setImageViewBitmap(R.id.app_icon, bitmap);

            if(apps.get(position).isEnabled) {
                remoteView.setViewVisibility(R.id.app_status, View.GONE);
            } else {
                remoteView.setViewVisibility(R.id.app_status, View.VISIBLE);

            }

            Bundle extras = new Bundle();
            extras.putString("package", apps.get(position).packageName);
            extras.putBoolean("from_widget", true);
            Intent fillIntent = new Intent();
            fillIntent.putExtras(extras);
            remoteView.setOnClickFillInIntent(R.id.app_col, fillIntent);




        } catch (Exception e) {
            e.printStackTrace();
        }







        return remoteView;
    }


    @Override
    public int getViewTypeCount() {
        // TODO Auto-generated method stub

        return 1;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        DBHelper dbHelper = new DBHelper(context);
        apps = dbHelper.getRecentApps("", true);
        dbHelper.close();


    }

    @Override
    public void onDestroy() {

    }



}

