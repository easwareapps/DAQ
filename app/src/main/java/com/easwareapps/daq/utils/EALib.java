package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import com.easwareapps.daq.R;

import java.io.File;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p>
 * <p>
 * DAQ
 * Created on 10/7/17.
 */

public class EALib {



    public Bitmap getIcon(Context context, String packageName, int width, int height)
            throws Exception{

        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        Drawable icon = p.applicationInfo.loadIcon(pm);

        if(width == -1 || height == -1){
            width = icon.getIntrinsicWidth();
            height = icon.getIntrinsicHeight();
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        icon.draw(canvas);
        return bitmap;


    }



    public boolean getAppsCurrentState(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.enabled;
    }

    public String getAppName(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.loadLabel(pm).toString();
    }

    public Bitmap createCustomIcon(Bitmap pkgIcon, Bitmap daqIcon) {


        Bitmap resultBitmap = Bitmap.createBitmap(pkgIcon.getWidth(), pkgIcon.getHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(resultBitmap);

        c.drawBitmap(pkgIcon, 0, 0, null);

        Paint p = new Paint();
        //p.setAlpha(127);

        c.drawBitmap(daqIcon, pkgIcon.getWidth()-daqIcon.getWidth(),
                pkgIcon.getHeight() - daqIcon.getHeight(), p);
        return resultBitmap;
    }

    public boolean isRooted() {

        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("widget_app_layout-keys")) {
            return true;
        }

        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
        }

        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }


    private static boolean canExecuteCommand(String command) {
        boolean executedSuccessfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccessfully = true;
        } catch (Exception e) {
            executedSuccessfully = false;
        }

        return executedSuccessfully;
    }



}
