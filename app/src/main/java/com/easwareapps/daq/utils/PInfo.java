package com.easwareapps.daq.utils;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by vishnu on 19/10/16.
 */
public class PInfo {
    public String appName = "";
    public String packageName = "";
    public String activity = "";
    public Drawable icon;
    public boolean selected = false;
    public boolean isSystemApp = true;
    public boolean isEnabled;
    public boolean comandExecuted = false;

}
