package com.easwareapps.daq.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.view.View;

import com.easwareapps.daq.R;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * EA-BulkReinstaller
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class DaqPref {

    Context context;
    static DaqPref instance;




    public static DaqPref getInstance(Context context) {

        if(instance == null) {
            instance = new DaqPref(context);
        }
        return instance;

    }
    public DaqPref(Context context) {
        this.context = context;
    }

    public void setPref(String key, String value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setPref(String key, boolean value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setPref(String key, int value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void setPref(String key, long value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public boolean shouldShowAd() {
        return (getPref(context.getString(R.string.key_disable_ad_till),
                System.currentTimeMillis()) <= System.currentTimeMillis());
    }

    public int getPref(String key, int value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        return pref.getInt(key, value);
    }

    public long getPref(String key, long value){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        return pref.getLong(key, value);
    }

    public boolean getPref(String key, boolean def){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        return pref.getBoolean(key, def);
    }

    public String getPref(String key, String def){
        SharedPreferences pref =context.getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);
        return pref.getString(key, def);
    }


    public int getTheme(boolean actionbar) {
        if (getPref(context.getString(R.string.key_enable_dark_theme), false)) {
            return (actionbar)?R.style.AppTheme_Dark:R.style.AppThemeDark_NoActionBar;
        }
        return (actionbar)?R.style.AppTheme:R.style.AppTheme_NoActionBar;
    }

    public int getSelectionColor() {

        if (getPref(context.getString(R.string.key_enable_dark_theme), false)) {
            return Color.GRAY;
        }
        return Color.GRAY;
    }

    public int getNormalColor() {

        if (getPref(context.getString(R.string.key_enable_dark_theme), false)) {
            return Color.BLACK;
        }
        return Color.WHITE;
    }

    public boolean shouldShowNotification(boolean newState) {
        int pref = getPref(context.getString(R.string.show_notification), 0);
        switch (pref) {
            case 0:
                return true;
            case 1:
                return !newState;
            case 2:
                return newState;
            case 3:
                return false;
        }
        return false;
    }

    public boolean shouldShowConfirmation(boolean currentState) {
        int pref = getPref(context.getString(R.string.shortcut_action), 0);
        switch (pref) {
            case 0:
                return true;
            case 1:
                return false;
            case 2:
                return false;
            case 3:
                return currentState;
            case 4:
                return !currentState;
        }
        return false;
    }

    public boolean shouldOpen() {
        int pref = getPref(context.getString(R.string.shortcut_action), 0);
        switch (pref) {
            case 2:
                return true;
            default:
                return false;
        }
    }
}
