package com.easwareapps.daq;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.easwareapps.daq.utils.DaqPref;
import com.easwareapps.daq.utils.EALib;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.utils.PackageStateChanger;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class QuickChangeActivity extends AppCompatActivity implements PackageStateChanger.CommandFinishedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.getBoolean("from_widget", false)) {
            final String packageName = bundle.getString("package");
            try {
                final boolean currentState = new EALib().getAppsCurrentState(getApplicationContext(), packageName);
                changeAppState(packageName, currentState, !currentState);

            } catch (Exception e) {
                e.printStackTrace();
            }
            //finish();
        } else {
            final String packageName = intent.getStringExtra("packageName");
            setTheme(R.style.Transparent);
            setContentView(R.layout.activity_quick_launch);



            try {
                final boolean currentState = new EALib().getAppsCurrentState(getApplicationContext(), packageName);
                int negString = currentState ? R.string.disable_app : R.string.enable_app;
                int msg = currentState ? R.string.app_is_enabled_what_to_do : R.string.app_is_disabled_what_to_do;

                ((TextView) findViewById(R.id.description)).setText(String.format(getString(msg), new EALib().getAppName(QuickChangeActivity.this, packageName)));
                if (DaqPref.getInstance(getApplicationContext()).shouldShowConfirmation(currentState)) {
                    findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });

                    ((Button) findViewById(R.id.change_state)).setText(negString);
                    findViewById(R.id.change_state).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            changeAppState(packageName, currentState, false);
                        }
                    });

                    findViewById(R.id.open).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!currentState) {
                                changeAppState(packageName, currentState, true);
                            } else {
                                openApp(packageName);
                                finish();
                            }
                        }
                    });
                    return;


                } else {
                    boolean shouldOpen = !currentState && DaqPref.getInstance(getApplicationContext()).shouldOpen();
                    changeAppState(packageName, currentState, shouldOpen);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        }
    }



    public void changeAppState(String packageName, boolean currentState, boolean shouldOpen) {
        PackageStateChanger psc = new PackageStateChanger(QuickChangeActivity.this,
                QuickChangeActivity.this, shouldOpen);
        PInfo info = new PInfo();
        info.packageName = packageName;
        info.isEnabled = currentState;
        psc.execute(info);
    }



    private void openApp(String packageName) {
        PackageManager pm = getPackageManager();
        Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
        launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(launchIntent);
    }

    public void finishApp(View v) {
        finish();
    }

    @Override
    public void onCommandFinished(boolean status, String packageName, boolean shouldOpen) {
        if(status && shouldOpen) {
            openApp(packageName);
        }
        finish();
    }

    @Override
    public void onUninstallFinished(boolean status, String packageName) {

    }
}
