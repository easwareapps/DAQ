package com.easwareapps.daq.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.utils.PackageStateChanger;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ is a simple app for disable apps quickly
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



public class EAReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        try {

            int appId = intent.getIntExtra("app_id", -1);
            if (appId > 0) {
                Log.d("CLEAR NOTIFICATIon", String.valueOf(appId) + "!!");
                new EANotificationManager().clearNotification(context, appId);
            }
            if (intent.getAction() != null && intent.getAction().equals("disable_app")) {
                String packageName = intent.getStringExtra("package");
                Log.d("PACKAGE", packageName);

                if (appId > 0) {
                    PackageStateChanger psc = new PackageStateChanger(context, null,  false);
                    PInfo pInfo = new PInfo();
                    pInfo.packageName = packageName;
                    pInfo.isEnabled = true;
                    psc.execute(pInfo);

                }

            } else if (intent.getAction() != null && intent.getAction().equals("enable_app")) {
                String packageName = intent.getStringExtra("package");
                Log.d("PACKAGE", packageName);

                if (appId > 0) {
                    PackageStateChanger psc = new PackageStateChanger(context, null,  false);
                    PInfo pInfo = new PInfo();
                    pInfo.packageName = packageName;
                    pInfo.isEnabled = false;
                    psc.execute(pInfo);
                }

            }  else if (intent.getAction() != null && intent.getAction().equals("open_app")) {
                String packageName = intent.getStringExtra("package");
                Log.d("PACKAGE", packageName);
                if (!getPackageStatus(context, packageName)) {
                    PackageStateChanger psc = new PackageStateChanger(context, null,  true);
                    PInfo pInfo = new PInfo();
                    pInfo.packageName = packageName;
                    pInfo.isEnabled = false;
                    psc.execute(pInfo);

                } else {
                    openApp(context, packageName);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void openApp(Context context, String packageName) throws Exception{
        PackageManager pm = context.getPackageManager();
        Intent launchIntent = new Intent(Intent.ACTION_MAIN, null);
        launchIntent.setComponent(pm.getLaunchIntentForPackage(packageName).resolveActivity(pm));
        launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launchIntent);
    }

    private boolean getPackageStatus(Context context, String packageName) throws Exception {
        PackageManager pm = context.getPackageManager();
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.enabled;
    }


}
