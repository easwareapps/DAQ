package com.easwareapps.daq.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.easwareapps.daq.R;
import com.easwareapps.daq.utils.Common;


/**
 * ॐ
 * लोकाः समस्ताः सुखिनो भवन्तु॥
 * <p/>
 * Quoter
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class EANotificationManager {


    String CHANNEL_ID = "com.easwareapps.daq";
    public void showNotifications(Context context, String packageName, String appName,
                                 int appId, boolean state) {

        PowerManager.WakeLock wl = null;
        try {
            PowerManager powerManager = (PowerManager) context
                    .getSystemService(Context.POWER_SERVICE);
            wl = powerManager.newWakeLock(
                    PowerManager.PARTIAL_WAKE_LOCK, "");
            wl.acquire();
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
        try {

            //int appId = Integer.parseInt(packageName.hashCode() + "".replace("-", ""), 32);
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            try {
                notificationManager.cancel(appId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            android.support.v4.app.NotificationCompat.Builder nb =
                    new android.support.v4.app.NotificationCompat.Builder(context, CHANNEL_ID);
            if(state)
                nb.setContentTitle(context.getString(R.string.app_enabled, appName));
            else
                nb.setContentTitle(context.getString(R.string.app_disabled, appName));
            nb.setContentText(context.getString(R.string.open_app));
            SharedPreferences pref = context.getSharedPreferences(Common.PACKAGE_NAME,
                    Context.MODE_PRIVATE);
            nb.setOngoing(pref.getBoolean(Common.PERSISTENT_NOTIFICATION, false));

            Intent openIntent = new Intent(context, EAReceiver.class);
            openIntent.setAction("open_app");
            openIntent.putExtra("package", packageName);
            PendingIntent piOpen = PendingIntent.getBroadcast(context,
                    appId + 70000, openIntent, PendingIntent.FLAG_CANCEL_CURRENT);



            Intent dismissIntent = new Intent(context, EAReceiver.class);
            dismissIntent.setAction("dismiss");
            dismissIntent.putExtra("app_id", appId);
            openIntent.putExtra("package", packageName);
            PendingIntent piDismiss = PendingIntent.getBroadcast(context,
                    appId + 30000, dismissIntent, PendingIntent.FLAG_CANCEL_CURRENT);





            nb.setContentIntent(piOpen);
            if(state) {
                Intent disableIntent = new Intent(context, EAReceiver.class);
                disableIntent.setAction("disable_app");
                disableIntent.putExtra("app_id", appId);
                disableIntent.putExtra("package", packageName);
                PendingIntent piDisable = PendingIntent.getBroadcast(context,
                        appId, disableIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                nb.addAction(R.drawable.ic_disabled, context.getString(R.string.disable), piDisable);
            } else {
                Intent enableIntent = new Intent(context, EAReceiver.class);
                enableIntent.setAction("enable_app");
                enableIntent.putExtra("app_id", appId);
                enableIntent.putExtra("package", packageName);
                PendingIntent piDisable = PendingIntent.getBroadcast(context,
                        appId, enableIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                nb.addAction(R.drawable.ic_tick, context.getString(R.string.enable_app), piDisable);
            }
            nb.addAction(R.drawable.ic_dismiss, context.getString(R.string.dismiss), piDismiss);

            PackageManager pm = context.getPackageManager();
            PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            Drawable icon = p.applicationInfo.loadIcon(pm);
            Bitmap bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight()
                    , Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            icon.draw(canvas);
            nb.setLargeIcon(bitmap).setSmallIcon(R.mipmap.ic_ni);
            notificationManager.notify(appId, nb.build());


            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                        context.getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }



            notificationManager.notify(appId , nb.build());
        } catch (Exception e) {
            //Log.d("OOPS!!!!", "OOPS!!!!");
            e.printStackTrace();
        }
        if (wl != null)
            wl.release();
    }


    public void clearNotification(Context context, int id) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            notificationManager.cancel(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
