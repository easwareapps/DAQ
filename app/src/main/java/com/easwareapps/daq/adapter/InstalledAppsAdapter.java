package com.easwareapps.daq.adapter;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v7.view.menu.ExpandedMenuView;
import android.util.Log;
import android.support.v7.widget.RecyclerView;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easwareapps.daq.MainActivity;
import com.easwareapps.daq.fragments.InstalledAppsFragment;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.DaqPref;
import com.easwareapps.daq.utils.BitmapManager;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.R;
import com.easwareapps.daq.utils.PackageStateChanger;

import java.util.ArrayList;


public class InstalledAppsAdapter extends RecyclerView.Adapter<InstalledAppsAdapter.ViewHolder>  {


    public boolean selectionStarted = false;
    private Context context;
    private ArrayList<PInfo> appsDetails;
    public static LruCache<String, Bitmap> cache;
    private int iconSize;
    DaqPref pref;
    private ActionsTriggeredListener actionsTriggeredListener;
    private int type;
    private PackageManager pm;




    public static InstalledAppsAdapter getInstance(ArrayList<PInfo> apps, Context context,
                                                   int iconSize, RecyclerView rv1,
                                                   ActionsTriggeredListener actionsTriggeredListener,
                                                   int type) {
        InstalledAppsAdapter instance = new InstalledAppsAdapter();



        instance.pref = DaqPref.getInstance(context);
        instance.appsDetails = apps;
        instance.iconSize = iconSize;
        instance.context = context;
        instance.actionsTriggeredListener = actionsTriggeredListener;
        instance.type = type;
        instance.pm = context.getPackageManager();

        return instance;
    }

    public InstalledAppsAdapter(){

        final int maxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
        int cacheSize = maxMemory/4;
        cache = new LruCache<String, Bitmap>(cacheSize){

            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() - value.getHeight();
            }

        };


    }

    public void requestUpdate() {

        notifyDataSetChanged();
    }

    public void confirmAndUninstallApps() {
        actionsTriggeredListener.confirmAndUninstallApps(appsDetails);
    }

    public void createShortcuts() {
        actionsTriggeredListener.createShortcuts(appsDetails);
    }

    public void toggleApps() {
        actionsTriggeredListener.toggleApps(appsDetails);
    }

    public void setTriggerListener(ActionsTriggeredListener triggerListener) {
        this.actionsTriggeredListener = triggerListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener, View.OnCreateContextMenuListener  {
        ImageView icon = null;
        ImageView status = null;
        TextView name = null;
        View mainView;
        public ViewHolder(View view){
            super(view);
            mainView = view;
            icon = (ImageView)view.findViewById(R.id.app_icon);
            status = (ImageView)view.findViewById(R.id.app_status);
            status = (ImageView)view.findViewById(R.id.app_status);
            name = (TextView)view.findViewById(R.id.app_name);
            view.setOnLongClickListener(this);
            mainView.setOnClickListener(this);


        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            //menuInfo is null
            menu.add(R.string.action_settings);
            menu.add(R.string.uninstall_app);
        }


        @Override
        public void onClick(final View view) {

            if(!selectionStarted) {
                PInfo info = appsDetails.get(getAdapterPosition());
                int pos = getAdapterPosition();
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
                view.startAnimation(animation);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(250);
                        } catch (Exception e) {

                        }
                    }
                });
                if(actionsTriggeredListener != null)
                    actionsTriggeredListener.toggleAppState(info.packageName, info.appName);
//                if() {
//                    try {
//                        appsDetails.get(pos).isEnabled = getPackageStatus(info.packageName);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    DBHelper dbHelper = new DBHelper(context);
//                    dbHelper.updateAppStatus(info.packageName, appsDetails.get(pos).isEnabled);
//                    dbHelper.close();
//                    notifyItemChanged(getAdapterPosition());
//                }
                return;

            } else {
                appsDetails.get(getAdapterPosition()).selected = !appsDetails.get(getAdapterPosition()).selected;
                view.setSelected(appsDetails.get(getAdapterPosition()).selected);
                if (appsDetails.get(getAdapterPosition()).selected) {
                    view.setBackgroundColor(pref.getSelectionColor());
                } else {
                    view.setBackgroundColor(pref.getNormalColor());
                }
            }


        }

        @Override
        public boolean onLongClick(View view) {
            if(!selectionStarted) {
                selectionStarted = true;
                onClick(view);
                actionsTriggeredListener.changeActionMode(true);
                return true;
            }
            return true;

        }


    }
    private int getAppIndex(String packageName) {
        int index = 0;
        for (PInfo info: appsDetails) {
            if(info.packageName.equals(packageName)) {
                return index;
            }
            index++;
        }
        return -1;
    }

    public void removeApp(String packageName) {
        int pos = getAppIndex(packageName);
        if (pos >= 0) {
            notifyItemRemoved(pos);
            appsDetails.remove(pos);
        }


    }

    public void updateApp(String packageName) {
        int pos = getAppIndex(packageName);
        if (pos >= 0) {
            try {
                appsDetails.get(pos).isEnabled = getPackageStatus(packageName);
                DBHelper dbHelper = new DBHelper(context);
                dbHelper.updateAppStatus(packageName, getPackageStatus(appsDetails.get(pos).packageName));
                dbHelper.close();
            } catch (Exception e) {

            }
            notifyItemChanged(pos);
            //appsDetails.remove(pos);
        }


    }






    public void removeAllSelection() {
        for(PInfo pkg: appsDetails) {
            pkg.selected = false;
        }
        notifyDataSetChanged();
    }

    public void selectAll() {
        for(PInfo pkg: appsDetails) {
            pkg.selected = true;
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if(InstalledAppsFragment.NO_COLUMNS == 1) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_view_single_row,
//                    parent, false);
//            return new ViewHolder(view);
//        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_view,
                    parent, false);
            return new ViewHolder(view);
//        }
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            PInfo pInfo = appsDetails.get(position);
            holder.name.setText(pInfo.appName);
            final Bitmap bitmap = cache.get(pInfo.packageName);
            holder.icon.setLayoutParams(new RelativeLayout.LayoutParams(iconSize, iconSize));
            if(!holder.name.getText().equals(pInfo.appName)) {
                holder.icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }



            holder.status.setLayoutParams(new RelativeLayout.LayoutParams(iconSize, iconSize));
            if(pInfo.isEnabled != (holder.status.getVisibility() == View.GONE)) {
                if (pInfo.isEnabled) {
                    holder.status.setVisibility(View.GONE);
                } else {

                    holder.status.setVisibility(View.VISIBLE);
                }
            }
            if (bitmap != null) {
                holder.icon.setImageBitmap(bitmap);
            } else {
                Resources resource = context.getResources();
                BitmapManager bm = new BitmapManager(holder.icon, context,
                        InstalledAppsFragment.iconSize);
                bm.setPackageName(pInfo.packageName);
                final BitmapManager.AsyncDrawable asyncDrawable =
                        new BitmapManager.AsyncDrawable(resource, null, bm);
                holder.icon.setImageDrawable(asyncDrawable);
                bm.execute(position);
            }
            holder.mainView.setSelected(pInfo.selected);
            if (pInfo.selected) {
                holder.mainView.setBackgroundColor(pref.getSelectionColor());
            } else {
                holder.mainView.setBackgroundColor(pref.getNormalColor());
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }

    }

    private boolean getPackageStatus(String packageName) throws Exception {
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.enabled;
    }


    @Override
    public int getItemCount() {
        return appsDetails.size();
    }

    public interface ActionsTriggeredListener {
        void toggleAppState(String packageName, String appName);
        void confirmAndUninstallApps(ArrayList<PInfo> packages);
        void createShortcuts(ArrayList<PInfo> packages);
        void toggleApps(ArrayList<PInfo> packages);
        void changeActionMode(boolean state);
    }

}
