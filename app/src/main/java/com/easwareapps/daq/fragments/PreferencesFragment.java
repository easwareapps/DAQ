package com.easwareapps.daq.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;


import com.easwareapps.daq.BuildConfig;
import com.easwareapps.daq.R;
import com.easwareapps.daq.SettingsActivity;



public class PreferencesFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    SharedPreferences preferences;
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        PreferenceManager manager = getPreferenceManager();
        manager.setSharedPreferencesName("com.easwareapps.daq");
        addPreferencesFromResource(R.xml.settings);

        preferences = getActivity().getSharedPreferences("com.easwareapps.daq",
                Context.MODE_PRIVATE);


        Preference refer = (Preference) findPreference("rate");
        refer.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setDataAndType(Uri.parse("test"), "text/plain");
                //i.putExtra()
                return false;
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        preferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        preferences.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

        if(s.equals(getString(R.string.key_enable_dark_theme))) {
            getActivity().recreate();
        }

    }

    public void refer(View v) {
        Toast.makeText(getActivity(), "refer on PA", Toast.LENGTH_LONG).show();

    }

    public void gotoPlayStore(View v) {
        Toast.makeText(getActivity(), "Ps on PA", Toast.LENGTH_LONG).show();
    }
}
