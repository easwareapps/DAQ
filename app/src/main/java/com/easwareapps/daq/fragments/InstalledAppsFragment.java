package com.easwareapps.daq.fragments;


import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.easwareapps.daq.MainActivity;
import com.easwareapps.daq.R;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.PInfo;
import com.easwareapps.daq.adapter.InstalledAppsAdapter;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * EA-BulkReinstaller
 * Copyright (C) 2016  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class InstalledAppsFragment extends Fragment {

    ArrayList<PInfo> res = new ArrayList<>();
    RecyclerView apps;
    InstalledAppsAdapter allAppsAdapter;
    InstalledAppsAdapter recentAppsAdapter;
    InstalledAppsAdapter disabledAppsAdapter;
    public String searchText = "";
    public boolean isSearchViewExpanded = false;
    public static int iconSize = -1;
    int type = -1;
    DBHelper db;
    AppsLoader appsLoader = null;
    InstalledAppsAdapter.ActionsTriggeredListener triggerListener;
    public static int NO_COLUMNS;
    PackageManager pm;
    SwipeRefreshLayout swipe;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_installed_apps, container, false);
        apps = (RecyclerView) rootView.findViewById(R.id.appsList);
        setNoColumns();
        iconSize = getIconSize();
        GridLayoutManager glm = new GridLayoutManager(getActivity(), NO_COLUMNS);
        apps.setHasFixedSize(true);
        apps.setLayoutManager(glm);
        apps.setOnScrollListener(onScrollListener);
        new AppsLoader().execute("");

        try {
            swipe = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
            swipe.setRefreshing(true);
            swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            reloadApps(searchText, true);
                        }
                    }
            );
            //swipe.
        } catch (Exception e) {
            e.printStackTrace();
        }


        return rootView;
    }


    public void reloadApps(String search, boolean force) {
        //apps.setAdapter(null);
        if(appsLoader != null) {
            try {
                appsLoader.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

        try {
            appsLoader = new AppsLoader();
            setNoColumns();
            if(NO_COLUMNS <=1) {
                NO_COLUMNS = 2;
            }
            GridLayoutManager glm = new GridLayoutManager(getActivity(), NO_COLUMNS);
            apps.setLayoutManager(glm);
            apps.setHasFixedSize(true);
            if (force)
                appsLoader.execute(search, "");
            else
                appsLoader.execute(search);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static InstalledAppsFragment getInstance(
            InstalledAppsAdapter.ActionsTriggeredListener triggerListener, int type) {
        InstalledAppsFragment iaf = new InstalledAppsFragment();
        iaf.triggerListener = triggerListener;
        iaf.type = type;
        return iaf;
    }

    public void selectAll() {
        getCurrentAdapter().selectAll();

    }

    public void removeAllSelection() {
        getCurrentAdapter().removeAllSelection();

    }

    public void confirmAndUninstallApps() {
        getCurrentAdapter().confirmAndUninstallApps();
    }

    public void createShortcuts() {
        getCurrentAdapter().createShortcuts();
    }

    public void toggleApps() {
        getCurrentAdapter().toggleApps();
    }

    public InstalledAppsAdapter getCurrentAdapter() {
        switch (this.type) {
            case MainActivity.ALL_APPS:
                return allAppsAdapter;
            case MainActivity.RECENT_APPS:
                return recentAppsAdapter;
        }
        return null;
    }

    public ArrayList<PInfo> getAppsList() {
        return  res;
    }

    public void setScrollPosition(float scrollPosition) {
        apps.setScaleY(scrollPosition);
    }


    private class AppsLoader extends AsyncTask<String, Void, Void> {
        boolean forceReload = false;
        ArrayList<PInfo> result = new ArrayList<>();
        @Override
        protected void onPreExecute() {
            Log.d("TIME", System.currentTimeMillis() + " !");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... data) {
            forceReload =  (data.length == 2);
            getApps(data[0]);
            return null;
        }

        void getApps(String search) {

            result.clear();
            search = search.trim();
            if(db == null) {
                db = new DBHelper(getActivity());
            }
            if(forceReload && type == MainActivity.ALL_APPS) {
                getAppsFromSystem();
            }


            if(type == MainActivity.ALL_APPS) {
                result = db.getAllApps(search, MainActivity.showSystemApp);
                if(result.size() == 0 && search.equals("")) {
                    getAppsFromSystem();
                    result = db.getAllApps(search, MainActivity.showSystemApp);
                }

            } else if (type == MainActivity.RECENT_APPS) {
                result = db.getRecentApps(search, MainActivity.showSystemApp);
            }
            try {
                db.close();
            } catch (Exception e) {
                e.getLocalizedMessage();
            }

        }

        private void getAppsFromSystem() {
            if(db == null) {
                db = new DBHelper(getActivity());
            }
            ArrayList<PInfo> result = new ArrayList<>();
            PackageManager pm = getActivity().getPackageManager();
            int flags = PackageManager.GET_META_DATA |
                    PackageManager.GET_SHARED_LIBRARY_FILES |
                    PackageManager.GET_GIDS;
            List<PackageInfo> packs;

            packs = pm.getInstalledPackages(flags);


            for (PackageInfo p : packs) {
                try {
                    PInfo newInfo = new PInfo();
                    newInfo.appName = p.applicationInfo.loadLabel(getActivity()
                            .getPackageManager()).toString();
                    newInfo.packageName = p.packageName;
                    ApplicationInfo ai = pm.getApplicationInfo(p.packageName, 0);
                    newInfo.isSystemApp = ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
                    newInfo.isEnabled = getPackageStatus(p.packageName);
                    result.add(newInfo);

                } catch (Exception e) {

                    Log.d("Package Exception", e.getLocalizedMessage());
                }
            }
            Collections.sort(result, new Comparator<PInfo>() {

                public int compare(PInfo p1, PInfo p2) {
                    return p1.appName.compareToIgnoreCase(p2.appName);
                }
            });
            db.addAppsToDB(result);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            res = result;
            super.onPostExecute(aVoid);
            try {
                if(swipe != null)
                    swipe.setRefreshing(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                apps.getRecycledViewPool().clear();
            } catch (Exception e) {

            }
            switch (type) {
                case MainActivity.ALL_APPS:

                    allAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps,
                            triggerListener, type);
                    apps.setAdapter(allAppsAdapter);
                    allAppsAdapter.requestUpdate();
                    break;
                case MainActivity.RECENT_APPS:

                    recentAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps,
                            triggerListener, type);
                    apps.setAdapter(recentAppsAdapter);
                    recentAppsAdapter.requestUpdate();
                    break;
                case MainActivity.DISABLED_APPS:

                    disabledAppsAdapter = InstalledAppsAdapter.getInstance(res,
                            getActivity(), getIconSize(), apps,
                            triggerListener, type);
                    apps.setAdapter(disabledAppsAdapter);
                    disabledAppsAdapter.requestUpdate();
                    break;
            }



        }
    }


    private int getIconSize() {
        DisplayMetrics dm = new DisplayMetrics();
        if (iconSize == -1) {

            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;
            if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                width = dm.heightPixels;
            }


            iconSize = width / NO_COLUMNS;
            iconSize = iconSize - (width/dm.densityDpi)*5;
        }
        Log.d("ICON", iconSize + "!!" );
        //iconSize = iconSize-(100);
        return iconSize;
    }

    private boolean getPackageStatus(String packageName) throws Exception {
        if(pm == null) {
            pm = getActivity().getPackageManager();
        }
        PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        return p.applicationInfo.enabled;
    }

//    public void closeActionMode() {
//        allAppsAdapter.closeActionMode();
//    }




    public void removeApp(String packageName) {
        switch (type) {
            case MainActivity.ALL_APPS:
                allAppsAdapter.removeApp(packageName);
                break;
            case MainActivity.RECENT_APPS:
                recentAppsAdapter.removeApp(packageName);
                break;
        }
    }

    public void update(String packageName) {
        try {
            switch (type) {
                case MainActivity.ALL_APPS:
                    allAppsAdapter.updateApp(packageName);
                    break;
                case MainActivity.RECENT_APPS:
                    recentAppsAdapter.updateApp(packageName);
                    break;
            }
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    public void setNoColumns() {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();

        if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            float dpWidth =  displayMetrics.heightPixels/displayMetrics.density;
            NO_COLUMNS = (int)dpWidth/72;
        } else {
            float dpWidth =  displayMetrics.widthPixels/displayMetrics.density;
            NO_COLUMNS = (int)dpWidth/84;



        }

        iconSize = -1;

    }

    public float getScrollPosition() {
        return apps.getScrollY();
    }

    public RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        boolean hideToolBar = false;
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (hideToolBar && !isSearchViewExpanded) {

                ((MainActivity)getActivity()).getSupportActionBar().hide();

            } else {

                ((MainActivity)getActivity()).getSupportActionBar().show();
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 20) {
                hideToolBar = true;

            } else if (dy < -5) {
                hideToolBar = false;

            }
        }
    };
}
