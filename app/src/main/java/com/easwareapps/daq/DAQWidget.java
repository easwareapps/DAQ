package com.easwareapps.daq;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.RemoteViews;

import com.easwareapps.daq.service.GridWidgetService;
import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.PInfo;

import java.util.ArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class DAQWidget extends AppWidgetProvider {



    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.d("WIDGET", "updating");
        for (int appWidgetId : appWidgetIds) {
            Log.d("WIDGET", "updating" + appWidgetId);
            Intent svcIntent=new Intent(context, GridWidgetService.class);

            svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            svcIntent.setData(Uri.parse(svcIntent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews widget = new RemoteViews(context.getPackageName(),
                    R.layout.daqwidget);

            widget.setRemoteAdapter(R.id.recent_apps_grid, svcIntent);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.recent_apps_grid);



            Intent startActivityIntent = new Intent(context, QuickChangeActivity.class);
            PendingIntent startActivityPendingIntent =
                    PendingIntent.getActivity(context, 0,
                            startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            widget.setPendingIntentTemplate(R.id.recent_apps_grid, startActivityPendingIntent);


            appWidgetManager.updateAppWidget(appWidgetId, widget);
            Log.d("WIDGET", "updated" + appWidgetId);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }




    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WIDGET", "REC " + AppWidgetManager.ACTION_APPWIDGET_UPDATE + "," + intent.getAction());
        if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(intent.getAction())) {

            Bundle extras = intent.getExtras();
            int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,  R.id.recent_apps_grid);



        }

        super.onReceive(context, intent);
    }

}

