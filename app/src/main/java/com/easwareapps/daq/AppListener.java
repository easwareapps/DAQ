package com.easwareapps.daq;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.easwareapps.daq.utils.DBHelper;
import com.easwareapps.daq.utils.PInfo;

/**
 * ************************************* ॐ ***********************************
 * **************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * DAQ
 * Copyright (C) 2017  vishnu
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AppListener extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            DBHelper db = new DBHelper(context);
            String packageName=intent.getData().getEncodedSchemeSpecificPart();
            if(intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                PackageManager pm = context.getPackageManager();
                PackageInfo p = pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
                PInfo info = new PInfo();
                info.packageName = packageName;
                info.activity = p.applicationInfo.loadLabel(context.getPackageManager()).toString();
                info.isSystemApp = p.applicationInfo.enabled;
                db.addApp(info);
            } else if(intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                db.removeApp(packageName);
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
